module.exports = {

  friendlyName: 'Validar inscripcion partido',

  description: 'Comprueba que el partido existe y el jugador no está inscrito ya en él.',

  inputs: {
    partido: {
      type: 'number'
    },
    jugador: {
      type: 'number'
    }
  },

  exits: {
    success: {
      description: 'All done.',
    },
  },

  fn: async function (inputs) {
    var partido = await Partido.findOne({codPartido: inputs.partido});
    if (typeof partido !== 'undefined'){ // Comprueba que existe el partido
      if(!partido.cancelado && !partido.reserva){ // Comprueba que el partido no esté cancelado ni tenga ya reserva
        var inscription = await JugadoresPartido.findOne({
          partido: inputs.partido,
          jugador : inputs.jugador
        });
        if(typeof inscription === 'undefined'){ // Comprueba que el jugador no esté inscrito ya en el partido
          return true;
        }
      }
    }
    return false;
  }
};

