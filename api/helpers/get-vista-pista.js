module.exports = {

  friendlyName: 'Get vista pista',

  description: '',

  inputs: {
    codPista: {
      type: 'number',
      allowNull: true
    }
  },

  exits: {
    success: {
      outputFriendlyName: 'Vista pista',
    },
  },

  fn: async function (inputs) {
    var pista = await Pista.findOne({codPista: inputs.codPista});
    if(typeof pista !== 'undefined'){
      var tipo = 'exterior';
      if(pista.interior){
        tipo = 'interior';
      }
      return ('Pista ' + pista.codPista + '. ' + 'Pista ' + tipo + ' de ' + pista.superficie + '.');
    }
    else {
      return '';
    }
  }
};

