/**
 * aplanar-array.js
 *
 * @description :: Función de apoyo que recibe un array de arrays de cualquier dimensión
 * y devuelve un solo array aplanado.
 */


module.exports = {


  friendlyName: 'Aplanar array',


  description: '',


  inputs: {
    arrNuevo: {
      type: 'ref'
    },
    arrViejo: {
      type: 'ref'
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {

    if(inputs.arrViejo == null){
      return inputs.arrNuevo;
    }

    inputs.arrNuevo.push(inputs.arrViejo[0]);

    return await sails.helpers.aplanarArray(inputs.arrNuevo, inputs.arrViejo[1]);
  }
};

