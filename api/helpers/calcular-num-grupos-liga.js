/**
 * calcular-num-grupos-liga.js
 *
 * @description :: Función recursiva que calcula cuantos jugadores deben ir en cada grupo de una liga, 
 * para realizar el mejor balanceo de jugadores posible.
 */

module.exports = {

  friendlyName: 'Calcular num grupos liga',

  description: '',

  inputs: {
    nJugadores: {
      type: 'number'
    }
  },

  exits: {
    success: {
      description: 'All done.',
    },
  },

  fn: async function (inputs) {

    if(inputs.nJugadores < 8){
      return [0];
    }

    if(inputs.nJugadores <= 12){
      return [inputs.nJugadores];
    }

    if(inputs.nJugadores < 16){
      return [12];
    }

    var num = await sails.helpers.repartirJugadoresGrupo(inputs.nJugadores);
    var resto = inputs.nJugadores - num;

    return [num, await sails.helpers.calcularNumGruposLiga(resto)];
  }
};

