module.exports = {

  friendlyName: 'Get partidos inscrito',

  description: 'Devuelve un array con los partidos en los que está inscrito el jugador',

  inputs: {
    codUsr:{
      type: 'number'
    }
  },

  exits: {
    success: {
      outputFriendlyName: 'Partidos inscrito',
    },
  },

  fn: async function (inputs) {
    var partidosYaInscrito = await JugadoresPartido.find({jugador: inputs.codUsr});
    var codsPartidoInscrito = [];
    for(i = 0; i < partidosYaInscrito.length; i++){
      codsPartidoInscrito.push(partidosYaInscrito[i].partido);
    }
    return codsPartidoInscrito;
  }
};

