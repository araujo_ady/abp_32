module.exports = {


  friendlyName: 'formatear fecha',

  description: 'Estandariza una fecha en formato español (DD-MM-AAAA)',

  inputs: {
    date:{
      type: 'ref'
    }
  },

  exits: {
    success: {
      description: 'All done.',
    },
  },

  fn: async function (inputs) {
    var fecha = new Date(inputs.date);
    var toret = fecha.getDate() + ' - ' + (fecha.getMonth()+1) + ' - ' + fecha.getFullYear();
    return toret;
  }
};

