module.exports = {


  friendlyName: 'Formatear fecha y hora',


  description: 'Estandariza y unifica el formato de una fecha cualquiera y una hora (int)',


  inputs: {
    date: {
      type: 'ref'
    },
    hora: {
      type: 'number'
    }
  },


  exits: {
    success: {
      description: 'All done.',
    },
  },


  fn: async function (inputs) {
    var fecha = new Date(inputs.date);
    var toret;
    switch (inputs.hora) {
      case 0:
        fecha.setHours('9', '0', '0');
        break;
      case 1:
        fecha.setHours('10', '30', '0');
        break;
      case 2:
        fecha.setHours('12', '00', '0');
        break;
      case 3:
        fecha.setHours('13', '30', '0');
        break;
      case 4:
        fecha.setHours('15', '00', '0');
        break;
      case 5:
        fecha.setHours('16', '30', '0');
        break;
      case 6:
        fecha.setHours('18', '00', '0');
        break;
      case 7:
        fecha.setHours('19', '30', '0');
        break;
    }
    toret = await sails.helpers.formatearFecha(fecha);
    toret += ' ' + fecha.toLocaleTimeString();
    return toret;
  }
};

