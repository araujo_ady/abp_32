/**
 * calcular-proxima-semana.js
 *
 * @description :: Función que dada una fecha, devuelve una fecha de la semana proxima.
 */

module.exports = {

    friendlyName: 'Calcular proxima semana',
  
    description: '',
  
    inputs: {
      fecha: {
        type: 'string'
      }
    },
  
    exits: {
      success: {
        description: 'All done.',
      },
    },
  
    fn: async function (inputs) {
  
      var f = inputs.fecha;
      var array = f.split('-');
      var dia = parseInt(array[2]);
      var mes = parseInt(array[1]);
      var anho = parseInt(array[0]);
      var diaMax;

      dia = dia+7;

      if(anho == 2020 && mes == 2){
        diaMax = 29;
      }
      else{
      switch(mes){
        case 1:
          diaMax = 31;
          break;
        case 2:
          diaMax = 28;
          break;
        case 3:
          diaMax = 31;
          break;
        case 4:
          diaMax = 30;
          break;
        case 5:
          diaMax = 31;
          break;
        case 6:
          diaMax = 30;
          break;
        case 7:
          diaMax = 31;
          break;
        case 8:
          diaMax = 31;
          break;
        case 9:
          diaMax = 30;
          break;
        case 10:
          diaMax = 31;
          break;
        case 11:
          diaMax = 30;
          break;
        case 12:
          diaMax = 31;
          break;
      }}
      if(dia > diaMax){
        dia = dia-diaMax;
        mes++;
      }

      return anho + "-" + mes + "-" + dia;

    }
  };
  
  