/**
 * get-nombre-entrenador.js
 *
 * @description :: Función que devuelve el nombre de un entrenador a partir de su Clave Primaria.
 */

module.exports = {

    friendlyName: 'Get nombre entrenador',
  
    description: '',
  
    inputs: {
        entrenador: {
        type: 'number'
      }
    },
  
    exits: {
      success: {
        outputFriendlyName: 'Nombre entrenador',
      },
  
    },
  
    fn: async function (inputs) {
      var entrenador = await Usuario.findOne({codUsr: inputs.entrenador});
      return (entrenador.nombre + ' ' + entrenador.apellidos);
    }
  };