/**
 * get-nombre-categoria.js
 *
 * @description :: Función que devuelve el nombre de una categoria a partir de su Clave Primaria.
 */

module.exports = {

  friendlyName: 'Get nombre categoria',

  description: '',

  inputs: {
    codCamp: {
      type: 'number'
    },
    codCategoria: {
      type: 'number'
    }
  },

  exits: {
    success: {
      outputFriendlyName: 'Nombre categoria',
    },
  },

  fn: async function (inputs) {
    var categoria = await Categoria.findOne({
      codCamp: inputs.codCamp,
      codCategoria: inputs.codCategoria
    });
    return (categoria.nombre);
  }
};

