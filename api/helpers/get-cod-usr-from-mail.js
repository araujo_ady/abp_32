module.exports = {

  friendlyName: 'Get cod usr from mail',

  description: 'Obtiene el código de usuario correspondiente a un email dado.',

  inputs: {
    email : {
      type: 'string'
    }
  },

  exits: {
    success: {
      outputFriendlyName: 'Cod usr from mail',
    },
  },

  fn: async function (inputs) {
    var user = await Usuario.findOne({correo: inputs.email});
    return (user.codUsr);
  }
};

