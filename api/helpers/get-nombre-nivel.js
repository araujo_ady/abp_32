/**
 * get-nombre-nivel.js
 *
 * @description :: Función que devuelve el nombre de un nivel a partir de su Clave Primaria.
 */

module.exports = {

  friendlyName: 'Get nombre categoria nivel',

  description: '',

  inputs: {
    codCamp: {
      type: 'number'
    },
    codCategoria: {
      type: 'number'
    },
    codNivel: {
      type: 'number'
    }
  },

  exits: {
    success: {
      outputFriendlyName: 'Nombre categoria nivel',
    },
  },

  fn: async function (inputs) {
    var param = {
      codCamp: inputs.codCamp,
      codCategoria: inputs.codCategoria,
      codNivel: inputs.codNivel
    };
    var catniv = await Nivel.findOne(param);
    return (catniv.nombre);
  }
};

