/**
 * get-nombre-escuela.js
 *
 * @description :: Función que devuelve el nombre de una escuela a partir de su Clave Primaria.
 */

module.exports = {

    friendlyName: 'Get nombre escuela',
  
    description: '',
  
    inputs: {
      escuela: {
        type: 'number'
      }
    },
  
    exits: {
      success: {
        outputFriendlyName: 'Nombre escuela',
      },
  
    },
  
    fn: async function (inputs) {
      var escuela = await EscuelaDeportiva.findOne({codEsc: inputs.escuela});
      return (escuela.nombre);
    }
  };
  