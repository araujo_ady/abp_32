var mailer = require('nodemailer');
module.exports = {
  friendlyName: 'Send mail',

  description: 'Enviar un correo electrónico',

  inputs: {
    to: {
      type: 'string'
    },
    subject: {
      type: 'string'
    },
    text: {
      type: 'string'
    }
  },

  exits: {
    success: {
      description: 'All done.',
    },
  },

  fn: async function (inputs) {
    var options = sails.config.custom.datosEmail;
    var mail = {
      from: options.auth.user,
      to: inputs.to,
      subject: inputs.subject,
      text: inputs.text
    };
    var transporter = mailer.createTransport(options);
    let result = await transporter.sendMail(mail);

    return result;
  }
};

