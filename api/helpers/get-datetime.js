module.exports = {

  friendlyName: 'Get datetime',

  description: 'Convierte una fecha (Date) y una hora (int) en tipo Date',

  inputs: {
    dia: {
      type: 'ref'
    },
    hora: {
      type: 'number'
    }
  },


  exits: {
    success: {
      outputFriendlyName: 'Datetime',
    },
  },


  fn: async function (inputs) {
    switch(inputs.hora){
      case 0:
        inputs.dia.setHours('9', '0', '0');
        break;
      case 1:
        inputs.dia.setHours('10', '30', '0');                                    break;
      case 2:
        inputs.dia.setHours('12', '00', '0');
        break;
      case 3:
        inputs.dia.setHours('13', '30', '0');
        break;
      case 4:
        inputs.dia.setHours('15', '00', '0');
        break;
      case 5:
        inputs.dia.setHours('16', '30', '0');
        break;
      case 6:
        inputs.dia.setHours('18', '00', '0');
        break;
      case 7:
        inputs.dia.setHours('19', '30', '0');
        break;
    }
    return inputs.dia;
  }
};

