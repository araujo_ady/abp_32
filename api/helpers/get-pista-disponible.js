module.exports = {

  friendlyName: 'Obtener pista disponible',

  description: 'Comprueba si hay alguna pista vacía y, si hay uno o más, devuelve su codPista. Si no, devuelve cero.',

  inputs: {
    fecha: {
      type: 'ref'
    },
    hora: {
      type: 'number'
    }
  },

  exits: {
    success: {
      outputFriendlyName: 'Pista disponible',
    },
  },

  fn: async function (inputs) {
    var datetime = {
      fecha: inputs.fecha,
      hora: inputs.hora
    };
    var pistas = await Pista.find();              // Número de pistas del club
    var reservas = await Reserva.find(datetime);  // Reservas ya realizadas
    if(pistas.length >= reservas.length){         // Si hay más pistas que reservas, hay pistas libres
      var codPistas = [];
      for(i = 0; i < pistas.length; i++){
        codPistas.push(pistas[i].codPista);
      }
      var pistasOcupadas = [];
      for(i = 0; i < reservas.length; i++){
        pistasOcupadas.push(reservas[i].codPista);
      }
      var pistasLibres = codPistas.filter(itr => !pistasOcupadas.includes(itr));
      return pistasLibres[0];
    }
    return 0;                                   // Si no hay más pistas que reservas, no hay pistas libres
  }
};

