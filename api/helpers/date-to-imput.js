module.exports = {

  friendlyName: 'date to imput',

  description: 'Convierte una fecha en una cadena con el formato de entrada de input date',

  inputs: {
    date:{
      type: 'ref'
    }
  },

  exits: {
    success: {
      description: 'All done.',
    },
  },

  fn: async function (inputs) {
    var fecha = new Date(inputs.date);
    var toret = fecha.getFullYear().toString();
    // El campo input date requiere dos cifras en el mes en el atributo value
    switch(fecha.getMonth()+1){
      case 1:
        toret += '-01';
        break;
      case 2:
        toret += '-02';
        break;
      case 3:
        toret += '-03';
        break;
      case 4:
        toret += '-04';
        break;
      case 5:
        toret += '-05';
        break;
      case 6:
        toret += '-06';
        break;
      case 7:
        toret += '-07';
        break;
      case 8:
        toret += '-08';
        break;
      case 9:
        toret += '-09';
        break;
      default:
        toret += '-' + (fecha.getMonth()+1).toString();
        break;
    }
    // El campo input date requiere dos cifras en el día en el atributo value
    switch(fecha.getDate()){
      case 1:
        toret += '-01';
        break;
      case 2:
        toret += '-02';
        break;
      case 3:
        toret += '-03';
        break;
      case 4:
        toret += '-04';
        break;
      case 5:
        toret += '-05';
        break;
      case 6:
        toret += '-06';
        break;
      case 7:
        toret += '-07';
        break;
      case 8:
        toret += '-08';
        break;
      case 9:
        toret += '-09';
        break;
      default:
        toret += '-' + fecha.getDate().toString();
        break;
    }
    return toret;
  }
};

