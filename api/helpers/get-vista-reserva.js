module.exports = {

  friendlyName: 'Get vista reserva',

  description: 'Devuelve una string formato "Pista X. Fecha" a partir de un codReserva dado',

  inputs: {
    codReserva: {
      type: 'number',
      allowNull: true
    }
  },

  exits: {
    success: {
      outputFriendlyName: 'Vista reserva',
    },
  },

  fn: async function (inputs) {
    var reserva = await Reserva.findOne({codReserva: inputs.codReserva});
    if(typeof reserva !== 'undefined'){
      //var fecha = await sails.helpers.formatearFechaHora(reserva.fecha, reserva.hora);
      //return ['Pista ' + reserva.codPista, fecha];
      return ('Pista ' + reserva.codPista);
    }
    else {
      //return ['',''];
      return '';
    }
  }
};

