/**
 * repartir-jugadores-grupo.js
 *
 * @description :: Función de apoyo para realizar el balanceo de jugadores en una liga regular.
 */

module.exports = {

  friendlyName: 'Repartir jugadores grupo',

  description: '',

  inputs: {
    nJugadores: {
      type: 'number'
    }
  },

  exits: {
    success: {
      description: 'All done.',
    }
  },

  fn: async function (inputs) {
    var i=2;
    var num=0;

    while(num<8 || num>12){
      num = inputs.nJugadores/i;
      i++;
    }

    return parseInt(num);
  }
};

