/**
 * get-nombre-usuario.js
 *
 * @description :: Función que devuelve el nombre completo de un usuario a partir de su Clave Primaria.
 */

module.exports = {

  friendlyName: 'Get nombre usuario',

  description: '',

  inputs: {
    codUsr: {
      type: 'number'
    }
  },

  exits: {
    success: {
      outputFriendlyName: 'Nombre usuario',
    },

  },

  fn: async function (inputs) {
    var user = await Usuario.findOne({codUsr: inputs.codUsr});
    if (typeof user !== 'undefined'){
      return (user.nombre + ' ' + user.apellidos);
    }
    return ('Desconocido');
  }
};

