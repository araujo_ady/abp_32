/**
 * get-nombre-campeonato.js
 *
 * @description :: Función que devuelve el nombre de un campeonato a partir de su Clave Primaria.
 */

module.exports = {

  friendlyName: 'Get nombre campeonato',

  description: '',

  inputs: {
    codCamp: {
      type: 'number'
    }
  },

  exits: {
    success: {
      outputFriendlyName: 'Nombre campeonato',
    },

  },

  fn: async function (inputs) {
    var camp = await Campeonato.findOne({codCamp: inputs.codCamp});
    return (camp.nombre);
  }
};

