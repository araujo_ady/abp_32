/**
 * User.js
 *
 * @description :: Modelo que gestiona los usuarios registrados en el sistema.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'usuario',
  attributes: {
    codUsr: {
      type: 'number',
      required: false,
      unique: true,
      autoIncrement: true
    },
    correo: {
      type: 'string',
      required: true,
      isEmail: true,
      columnType: 'varchar(320)'
    },
    password: {
      type: 'string',
      required: true,
      encrypt: true,
      columnType: 'varchar(90)'
    },
    nombre: {
      type: 'string',
      required: false,
      allowNull: true,
      columnType: 'varchar(30)'
    },
    apellidos: {
      type: 'string',
      required: false,
      allowNull: true,
      columnType: 'varchar(255)'
    },
    fechaNac: {
      type: 'ref',
      required: false,
      columnType: 'date'
    },
    genero: {
      type: 'string',
      required: true,
      columnType: 'varchar(10)',
      isIn: ['hombre','mujer']
    },
    esSocio: {
      type: 'boolean',
      required: false,
      defaultsTo: false,
      columnType: 'boolean'
    },
    tipo: {
      /*
      0 -> Usuario
      1 -> Entrenador
      2 -> Administrador
      */
      type: 'number',
      required: false,
      defaultsTo: 0,
      columnType: 'tinyint(3)',
      isIn: [0,1,2]
    },
    avatar: {
      type: 'string',
      required: false,
      defaultsTo: 'default',
      columnType: 'varchar(255)'
    },
    banco: {
      type: 'string',
      required: false,
      allowNull: true,
      columnType: 'varchar(255)'
    },
    hash: {
      type: 'string',
      required: false,
      allowNull: true,
      encrypt: true,
      columnType: 'varchar(255)'
    }
  },
  primaryKey: 'codUsr'
};
