/**
 * Enfrentamiento.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'enfrentamiento',
  attributes: {
    codEnfrentamiento: {
      type: 'number',
      required: false,
      unique: true,
      autoIncrement: true
    },
    codCamp: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codCategoria: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codNivel: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    fecha: {
      type: 'ref',
      required: true,
      columnType: 'date'
    },
    hora: {
      /*
      0 9:00
      1 10:30
      2 12:00
      3 13:30
      4 15:00
      5 16:30
      6 18:00
      7 19:30
      */
      type: 'number',
      required: true,
      isIn: [0,1,2,3,4,5,6,7],
      columnType: 'tinyint(3)'
    },
    fase: {
      /*
      0 -> Liga Regular
      1 -> Segunda fase
      2 -> Fase final
      */
      type: 'number',
      required: true,
      columnType: 'tinyint(3)',
      isIn: [0,1,2,3,4,5,6,7]
    },
    codReserva: {
      type: 'number',
      allowNull: true,
      columnType: 'int(11)'
    }
  },
  primaryKey: 'codEnfrentamiento'
};

