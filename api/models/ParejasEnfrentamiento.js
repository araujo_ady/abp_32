/**
 * ParejasEnfrentamiento.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'parejasenfrentamiento',
  attributes: {
    codEnfrentamiento: {
      type: 'number',
      required: true
    },
    codCamp: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codCategoria: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codNivel: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codGrupo: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    capitan: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    jugador: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    resultado: {
      type: 'number',
      required: false,
      allowNull: true,
      columnType: 'int(11)'
    }
  },
  primaryKey: 'codEnfrentamiento' // La clave compuesta se inserta en config/bootstrap.js
};

