module.exports = {
  tableName: 'precios',
  attributes: {
    codPrecio: {
      type: 'number',
      required: false,
      unique: true,
      autoIncrement: true
    },
    servicio: {
      type: 'string',
      required: true,
      columnType: 'varchar(50)'
    },
    precio: {
      type: 'number',
      required: true,
      columnType: 'float'
    },
    precioSocio: {
      type: 'number',
      required: true,
      columnType: 'float'
    }
  },
  primaryKey: 'codPrecio'
};
