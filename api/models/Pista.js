/**
 * Pista.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'pista',
  attributes: {
    codPista: {
      type: 'number',
      required: false,
      unique: true,
      autoIncrement: true
    },
    superficie: {
      type: 'string',
      columnType: 'varchar(20)',
      isIn: ['Cemento','Cesped','Hormigón Poroso','Resina Sintética'],
      required: true
    },
    interior: {
      type: 'boolean',
      required: true,
      columnType: 'boolean'
    }
  },
  primaryKey: 'codPista'
};

