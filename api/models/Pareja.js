/**
 * Pareja.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'pareja',
  attributes: {
    codCamp: {
      type: 'number',
      required: true
    },
    codCategoria: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codNivel: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codGrupo: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codUsr1: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codUsr2: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    estaFormada: {
      type: 'boolean',
      required: true
    },
    puntuacionLigaRegular: {
      type: 'number',
      required: true
    }
  },
  primaryKey: 'codCamp' // La clave compuesta se inserta en config/bootstrap.js
};

