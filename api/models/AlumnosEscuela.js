/**
 * AlumnosEscuela.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'alumnosescuela',
  attributes: {
    alumno: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    escuela: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    }
  },
  primaryKey: ('alumno','escuela') // La clave compuesta se inserta en config/bootstrap.js
};