/**
 * ParticipantePartidoPrivado.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'jugadorespartido',
  attributes: {
    jugador: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    partido: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    resultado: {
      type: 'number',
      required: false,
      columnType: 'int(11)'
    }
  },
  primaryKey: ('jugador','partido') // La clave compuesta se inserta en config/bootstrap.js
};

