/**
 * ContenidoDinamico.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'contenidodinamico',
  attributes: {
    codContDin:{
      type: 'number',
      required: false,
      unique: true,
      autoIncrement: true,
      columnType: 'int(11)'
    },
    titulo:{
      type: 'string',
      columnType: 'varchar(50)',
      required: true
    },
    reseña:{
      type: 'string',
      columnType: 'varchar(255)',
      required: true
    },
    fecha:{
      type: 'ref',
      columnType: 'date',
      required: true
    },
    texto:{
      type: 'string',
      columnType: 'text',
      required: true
    },
    multimedia:{
      type: 'string',
      columnType: 'varchar(255)',
      allowNull: true,
      required: false
    },
    link:{
      type: 'string',
      columnType: 'varchar(255)',
      allowNull: true,
      required: false
    }
  },
  primaryKey: 'codContDin'
};

