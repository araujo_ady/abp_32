/**
 * ReservasEscuela.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'reservasescuela',
  attributes: {
    codEsc: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codReserva: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    }
  },
  primaryKey: ('codEsc','codReserva') // La clave compuesta se inserta en config/bootstrap.js
};