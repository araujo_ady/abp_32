/**
 * Campeonato.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'campeonato',
  attributes: {
    codCamp: {
      type: 'number',
      required: false,
      unique: true,
      autoIncrement: true
    },
    nombre: {
      type: 'string',
      required: false,
      allowNull: true,
      columnType: 'varchar(25)'
    },
    fechaInicio: {
      type: 'ref',
      required: true,
      columnType: 'date'
    },
    fechaFin: {
      type: 'ref',
      required: true,
      columnType: 'date'
    }
  },
  primaryKey: 'codCamp'
};

