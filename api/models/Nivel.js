/**
 * Nivel.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'nivel',
  attributes: {
    codCamp: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codCategoria: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codNivel: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    nombre: {
      type: 'string',
      required: false,
      allowNull: true,
      columnType: 'varchar(25)'
    }
  },
  primaryKey: ('codCamp', 'codCategoria', 'codNivel') // La clave compuesta se inserta en config/bootstrap.js
};

