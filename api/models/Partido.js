/**
 * Partido.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'partido',
  attributes: {
    codPartido: {
      type: 'number',
      required: false,
      unique: true,
      autoIncrement: true
    },
    fecha: {
      type: 'ref',
      required: true,
      columnType: 'date'
    },
    hora: {
      /*
      0 9:00
      1 10:30
      2 12:00
      3 13:30
      4 15:00
      5 16:30
      6 18:00
      7 19:30
      */
      type: 'number',
      required: true,
      isIn: [0,1,2,3,4,5,6,7],
      columnType: 'tinyint(3)'
    },
    cancelado: {
      type: 'boolean',
      required: false,
      defaultsTo: false,
      columnType: 'boolean'
    },
    promotor: {
      type: 'number',
      columnType: 'int(11)'
    },
    codReserva: {
      type: 'number',
      required: false,
      columnType: 'int(11)',
      allowNull: true
    }
  },
  primaryKey: 'codPartido'
};

