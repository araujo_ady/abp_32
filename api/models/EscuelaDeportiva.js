/**
 * EscuelaDeportiva.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'escueladeportiva',
  migrate: 'drop',
  attributes: {
    codEsc: {
      type: 'number',
      unique: true,
      autoIncrement: true
    },
    entrenador: {
      type: 'number',
      columnType: 'int(11)'
    },
    nombre: {
      type: 'string',
      required: false,
      allowNull: true,
      columnType: 'varchar(25)'
    },
    fechaInicio: {
      type: 'ref',
      required: true,
      columnType: 'date'
    },
    fechaFin: {
      type: 'ref',
      required: true,
      columnType: 'date'
    },
    fecha: {
      type: 'ref',
      required: true,
      columnType: 'date'
    },
    hora: {
      /*
      0 9:00
      1 10:30
      2 12:00
      3 13:30
      4 15:00
      5 16:30
      6 18:00
      7 19:30
      */
      type: 'number',
      required: true,
      isIn: [0,1,2,3,4,5,6,7],
      columnType: 'tinyint(3)'
    },
    cupoMin: {
      type: 'number',
      required: false,
      columnType: 'int(11)'
    },
    cupoMax: {
      type: 'number',
      required: false,
      columnType: 'int(11)'
    },
    numClases: {
      type: 'number',
      required: false,
      columnType: 'int(11)'
    }
  },
  primaryKey: 'codEsc'
};

