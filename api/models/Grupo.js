/**
 * Grupo.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'grupo',
  attributes: {
    codCamp: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codCategoria: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codNivel: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    codGrupo: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    }
  },
  primaryKey: ('codCamp', 'codCategoria', 'codNivel', 'codGrupo') // La clave compuesta se inserta en config/bootstrap.js
};
