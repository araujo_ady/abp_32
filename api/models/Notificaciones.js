/**
 * Notificaciones.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'notificaciones',
  attributes: {
    codNot: {
      type: 'number',
      unique: true,
      required: false,
      autoIncrement: true
    },
    receptor: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    emisor: {
      type: 'number',
      required: true,
      columnType: 'int(11)'
    },
    tipo: {
      /*
      0 -> Propuesta de Pareja,
      1 -> Propuesta de Partido,
      2 -> Mensaje de sólo lectura,
      3 -> Propuesta de Clase
      4 -> Validación de alta de socio
      */
      type: 'number',
      required: true,
      columnType: 'tinyint(3)'
    },
    aux1: {
      type: 'number',
      columnType: 'int(11)',
    },
    aux2: {
      type: 'number',
      columnType: 'int(11)'
    },
    aux3: {
      type: 'number',
      columnType: 'int(11)'
    },
    aux4: {
      type: 'number',
      columnType: 'int(11)'
    },
    asunto: {
      type: 'string',
      allowNull: true,
      columnType: 'varchar(50)'
    },
    texto: {
      type: 'string',
      allowNull: true,
      columnType: 'text'
    },
    visto: {
      type: 'boolean'
    }
  },
  primaryKey: 'codNot'
};
