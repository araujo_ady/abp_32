module.exports = async function (req, res, proceed) {
  if (typeof req.session.codUsr !== 'undefined') {
    return proceed();
  }
  return res.redirect('/login');
};
