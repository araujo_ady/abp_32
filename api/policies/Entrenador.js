module.exports = async function (req, res, proceed) {
  if (typeof req.session.codUsr !== 'undefined') {
    if(req.session.tipo >= 1){
      return proceed();
    }
  }
  return res.redirect('/login');
};
