/**
 * PistaController
 *
 * @description :: Funciones necesarias para añadir o borrar pistas.
 * Es necesario tener alguna pista creada para que el resto de gestiones puedan funcionar.
 */

module.exports = {
  show: async function(req, res) {
    var records = await Pista.find();
    res.view({
      records: records,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  insert: async function(req, res) {
    res.view({
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  confirmInsert: async function(req, res) {
    var pista = {
      superficie : req.param('superficie'),
      interior: true
    };
    if(req.param('tipo') === 'Exterior'){
      pista.interior = false;
    }
    await Pista.create(pista);
    res.redirect('/gestionarpistas');
  },

  delete: async function(req, res) {
    var pista = await Pista.findOne({codPista: req.param('codPista')});
    res.view({
      record : pista,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  confirmDelete: async function(req, res) {
    await Pista.destroyOne({codPista: req.param('codPista')});
    res.redirect('/gestionarpistas');
  }
};

