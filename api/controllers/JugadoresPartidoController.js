/**
 * JugadoresPartidoController
 *
 * @description :: Permite inscribirse a un partido, muestra los partidos y permite desinscribirse.
 */

module.exports = {
  showPartidosAbiertos: async function(req, res) {
    var codPartidoInscrito = await sails.helpers.getPartidosInscrito(req.session.codUsr);
    var records = await Partido.find({
      fecha: {'>': new Date()},
      cancelado: false,
      codReserva: null
    });
    for(i = 0; i < records.length; i++){
      records[i].codReserva = await sails.helpers.getVistaReserva(records[i].codReserva);
      records[i].fecha = await sails.helpers.formatearFechaHora(records[i].fecha, records[i].hora);
      records[i].promotor = await sails.helpers.getNombreUsuario(records[i].promotor);
    }
    res.view({
      records: records,
      inscrito: codPartidoInscrito,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  insert: async function(req, res) {
    if(await sails.helpers.validarInscripcionPartido(req.param('codPartido'),req.session.codUsr)){
      var precio = await Precios.findOne({codPrecio: '1'}); // Consulta el precio del servicio
      var precioFinal = precio.precio;
      if(req.session.esSocio){                              // Si es socio, tiene descuentos
        precioFinal = precio.precioSocio;
      }
      res.view({
        codPartido: req.param('codPartido'),
        precio : precioFinal,
        session: req.session,
        notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
      });
    }
    else {
      return res.badRequest();
    }
  },

  confirmInsert: async function(req, res){
    var inscripcion = {
      jugador: req.session.codUsr,
      partido: req.param('codPartido')
    };
    if(await sails.helpers.validarInscripcionPartido(inscripcion.partido,inscripcion.jugador)){
      await JugadoresPartido.create(inscripcion); // Inscribe al jugador en el partido
      var inscripciones = await JugadoresPartido.find({ partido: inscripcion.partido });
      if(inscripciones.length === 4){
        var partido = await Partido.findOne({codPartido : inscripcion.partido});
        var pistaDisponible = await sails.helpers.getPistaDisponible(new Date(partido.fecha).toLocaleDateString(), partido.hora);
        var mensaje;
        if(!pistaDisponible){ // Si no hay pista disponible
          await JugadoresPartido.destroy({partido: inscripcion.partido}); // Borra las inscripciones
          await Partido.updateOne({
            codPartido : inscripcion.partido
          }).set({
            cancelado : true
          }); // Cancela el partido
          // Envia una notificación al usuario indicando que el partido queda cancelado
          mensaje = {
            receptor: 0,
            emisor: req.session.codUsr,
            tipo: 2,
            asunto: 'Partido cancelado',
            texto: 'El partido con horario ' +
              await sails.helpers.formatearFechaHora(partido.fecha, partido.hora) +
              ' ha sido cancelado por no haber disponibilidad de pistas.'
          };
        }
        else { // Si hay pista disponible para hacer la reserva
          var reserva = {
            fecha: partido.fecha,
            hora:  partido.hora,
            codUsr: partido.promotor,
            codPista: pistaDisponible
          };
          var nuevaReserva = await Reserva.create(reserva).fetch(); // Crea la reserva
          await Partido.updateOne({                                 // Vincula el partido con la reserva
            codPartido : inscripcion.partido
          }).set({
            codReserva : nuevaReserva.codReserva
          });
          // Envía una notificación avisando de que el partido está completo y se ha creado la reserva
          mensaje = {
            receptor: 0,
            emisor: req.session.codUsr,
            tipo: 2,
            asunto: 'Partido completo',
            texto: 'El partido con horario ' +
              await sails.helpers.formatearFechaHora(partido.fecha, partido.hora) +
              ' ya ha cubierto sus plazas. Podrás jugarlo en la fecha prevista en la pista ' + nuevaReserva.codPista + '.'
          };
        }
        var email = {
          receptor: 0,
          asunto: 'Tienes una nueva notificación',
          texto: 'Parece que tienes nuevas notificaciones sin leer. Entra a la aplicación para consultarlas.',
        };
        for(i = 0; i < inscripciones.length; i++){
          var usuario = await Usuario.findOne({codUsr: inscripciones[i].jugador});
          mensaje.receptor = usuario.codUsr;
          email.receptor = usuario.correo;
          await Notificaciones.create(mensaje);
          await sails.helpers.sendMail(email.receptor, email.asunto, email.texto);
        }
      }
      res.redirect('/inscribirseenpartido/mispartidos');
    }
    else {
      return res.badRequest();
    }
  },

  show: async function(req, res) { // Mis Partidos
    var partidos = await JugadoresPartido.find({
      jugador : req.session.codUsr
    });
    var codsPartido = [];
    for(i = 0; i < partidos.length; i++){
      codsPartido.push(partidos[i].partido);
    }
    var records = await Partido.find({
      codPartido: codsPartido,
      fecha: {'>': new Date()}
    });
    for(i = 0; i < records.length; i++){
      records[i].codReserva = await sails.helpers.getVistaReserva(records[i].codReserva);
      records[i].fecha = await sails.helpers.formatearFechaHora(records[i].fecha, records[i].hora);
      records[i].promotor = await sails.helpers.getNombreUsuario(records[i].promotor);
    }
    res.view({
      records: records,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  delete: async function(req, res) {
    res.view({
      codPartido: req.param('codPartido'),
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  confirmDelete: async function(req, res) {
    var partido = {
      codPartido: req.param('codPartido')
    };
    var inscripcion = {
      jugador: req.session.codUsr,
      partido: partido.codPartido
    };
    partido = await Partido.findOne(partido);
    if(partido.codReserva === null){
      await JugadoresPartido.destroyOne(inscripcion).exec((_err, _res) => {
        res.redirect('/inscribirseenpartido/mispartidos');
      });
    }
    else{
      return res.forbidden();
    }
  }
};
