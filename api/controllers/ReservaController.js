/**
 * ReservaController
 *
 * @description :: Permite realizar una reserva de una pista, elimianrla y mostrar las reservas activas.
 */

module.exports = {
  eligePista: async function(req, res) {
    var pistas = await Pista.find();
    res.view({
      records: pistas,
      session: req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  eligeHora: async function(req, res) {
    var pista = req.param('codPista');
    var diasOcupados = []; // Matriz de booleanos, cada posición es un día
    for(i = 0; i < 7; i++){
      var horasOcupadas = []; // Array de booleanos, cada posición es una hora
      var fecha = new Date();
      fecha.setDate(fecha.getDate() + i);
      for(j = 0; j < 8; j++){
        var reserva = await Reserva.findOne({
          codPista : pista,
          fecha    : fecha.toLocaleDateString(),
          hora     : j
        });
        if(typeof reserva !== 'undefined'){
          horasOcupadas.push(true); // La hora j del día i está ocupada
        }
        else{
          horasOcupadas.push(false); // La hora j del día i está libre
        }
      }
      diasOcupados.push(horasOcupadas);
    }
    res.view({
      pista: pista,
      horasDisponibles: diasOcupados,
      session: req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  insert: async function(req, res) {
    var nReservasActivas = await Reserva.count({
      codUsr: req.session.codUsr,
      fecha: { '>=': new Date().toLocaleDateString() }
    });
    if(nReservasActivas <= 5){ // No se puede tener más de cinco reservas activas
      var fecha = req.param('dia');
      var dia = new Date();
      dia.setDate(dia.getDate() + (fecha/10));
      var hoy = new Date();
      var hora = parseInt(fecha%10);
      var fechaReserva = await sails.helpers.getDatetime(dia, hora);
      var diasRestantes = Math.round((fechaReserva.getTime() - hoy.getTime())/ (1000*60*60*24));
      if(diasRestantes < 7){ // Solo se pueden crear reservas con menos de 7 días de antelación
        var reserva = {
          fecha: dia.toLocaleDateString(),
          hora: parseInt(fecha%10),
          codUsr: req.session.codUsr,
          codPista: req.param('codPista'),
        };
        var precio = await Precios.findOne({codPrecio: '2'}); // Consulta el precio del servicio
        var precioFinal = precio.precio;
        if(req.session.esSocio){                              // Si es socio, tiene descuentos
          precioFinal = precio.precioSocio;
        }
        res.view({
          error: false,
          reserva: reserva,
          precio: precioFinal,
          session: req.session,
          notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
        });
      }
      else {
        res.forbidden(); // No se pueden crear reservas con más de cinco días de antelación
      }
    }
    else { // No se puede tener más de cinco reservas activadas
      res.view({
        error: true,
        session: req.session,
        notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
      });
    }
  },

  confirmInsert: async function(req, res) {
    var reserva = {
      fecha: req.param('fecha'),
      hora: req.param('hora'),
      codUsr: req.session.codUsr,
      codPista: req.param('codPista')
    };
    var nPistas = await Pista.count(); // Número de pistas del club
    var nuevaReserva = await Reserva.create(reserva).fetch();
    var nReservas = await Reserva.count({
      fecha : new Date(nuevaReserva.fecha).toLocaleDateString(),
      hora : nuevaReserva.hora
    });
    if(nPistas === nReservas){ // Si están todas las pistas ya ocupadas a esa hora
      await Partido.update({
        fecha : new Date(nuevaReserva.fecha).toLocaleDateString(),
        hora : nuevaReserva.hora,
        codReserva: 0
      }).set({
        cancelado: true           // Cancela los partidos promocionados abiertos a esa hora
      });
      // !!!!Tiene que cargarse también los enfrentamientos!!!!
    }
    res.redirect('/gestionarreservas');
  },

  show: async function(req, res) {
    var records;
    if(req.session.tipo === 2){ // Si el usuario es el administrador, puede ver todas las reservas
      records = await Reserva.find({
        where:{
          fecha: {'>=': new Date().toLocaleDateString()}
        },
        sort: 'codReserva DESC'
      });
    }
    else {                      // Si no, solo puede ver las reservas a su nombre
      records = await Reserva.find({
        where:{
          codUsr: req.session.codUsr,
          fecha: {'>=': new Date().toLocaleDateString()}
        },
        sort: 'codReserva DESC'
      });
    }
    var hoy = new Date();
    for(i = 0; i < records.length; i++){
      var fechaReserva = await sails.helpers.getDatetime(new Date(records[i].fecha), records[i].hora);
      records[i].horasRestantes = Math.round((fechaReserva.getTime() - hoy.getTime())/ (1000*60*60));
      records[i].fecha = await sails.helpers.formatearFechaHora(records[i].fecha, records[i].hora);
      records[i].codUsr = await sails.helpers.getNombreUsuario(records[i].codUsr);
      records[i].codPista = await sails.helpers.getVistaPista(records[i].codPista);
    }
    res.view({
      records: records,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  delete: async function(req, res) {
    var codReserva = req.param('codReserva');
    var reserva = await Reserva.findOne({codReserva: codReserva});
    var hoy = new Date();
    var fechaReserva = await sails.helpers.getDatetime(new Date(reserva.fecha), reserva.hora);
    var horasRestantes = Math.round((fechaReserva.getTime() - hoy.getTime())/ (1000*60*60));
    if(horasRestantes <= 12){ // No se permite cancelar reservas con menos de doce horas de antelación
      res.forbidden();
    }
    else {
      res.view({
        codReserva: codReserva,
        notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
      });
    }
  },
  confirmDelete: async function(req, res) {
    var codReserva = req.param('codReserva');
    var reserva = await Reserva.findOne({codReserva: codReserva});
    var hoy = new Date();
    var fechaReserva = await sails.helpers.getDatetime(new Date(reserva.fecha), reserva.hora);
    var horasRestantes = Math.round((fechaReserva.getTime() - hoy.getTime())/ (1000*60*60));
    if(horasRestantes <= 12){ // No se permite cancelar reservas con menos de doce horas de antelación
      res.forbidden();
    }
    else {
      await Reserva.destroyOne({codReserva: codReserva}).exec((_err, _res) => {
        res.redirect('/gestionarreservas');
      });
    }
  }
};
