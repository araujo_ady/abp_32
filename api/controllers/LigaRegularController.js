/**
 * LigaRegularController
 *
 * @description :: Genera las distintas ligas regulares que forman cada campeonato y genera los enfrentamientos entre jugadores.
 */

module.exports = {
  categorias: async function(req, res) {
    var campeonato = await Campeonato.findOne({codCamp : req.param('codCamp')});
    var niveles = await Nivel.find({codCamp : campeonato.codCamp});
    for(i = 0; i < niveles.length; i++){
      niveles[i].nombreCategoria = await sails.helpers.getNombreCategoria(niveles[i].codCamp, niveles[i].codCategoria);
      niveles[i].nombreNivel = await sails.helpers.getNombreNivel(niveles[i].codCamp, niveles[i].codCategoria, niveles[i].codNivel);
      niveles[i].nEnfrentamientos = await Enfrentamiento.count({
        codCamp : niveles[i].codCamp,
        codCategoria : niveles[i].codCategoria,
        codNivel : niveles[i].codNivel
      });
    }
    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      campeonato: campeonato,
      records: niveles,
      session : req.session
    });
  },
  grupos: async function(req, res) {
    //Sacamos la lista de grupos de ese nivel
    var grupos = await Grupo.find({where : {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel')}});

    //Aqui para cada grupo guardamos el numero de partidos sin jugar (sets == null)
    var partidosSinJugar = [];
    var hayPartidosDePlayoffs = [];

    //Recorremos los grupos
    for(var i=0; i<grupos.length;i++){
      var numero = await ParejasEnfrentamiento.count({codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codGrupo: (i+1), resultado : null});

      var cuentame = await ParejasEnfrentamiento.find({codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codGrupo: (i+1)});
      var banderin = false;

      for(var z=0;z<cuentame.length;z++){
        var hayPlay = await Enfrentamiento.count({codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), fase: 1});

        if(hayPlay!=0){
          banderin = true;
        }
      }

      if(banderin == true){
        hayPartidosDePlayoffs.push(true);
      }else{
        hayPartidosDePlayoffs.push(false);
      }

      partidosSinJugar.push(numero);
    }

    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      hayPartidosDePlayoffs: hayPartidosDePlayoffs,
      partidosSinJugar: partidosSinJugar,
      codCamp: req.param('codCamp'),
      nombreCamp: req.param('nombreCamp'),
      codCategoria: req.param('codCategoria'),
      nombreCat: req.param('nombreCat'),
      codNivel: req.param('codNivel'),
      nombreNivel: req.param('nombreNivel'),
      records: grupos,
      session : req.session
    });
  },
  parejas: async function(req, res) {
    var parejas = await Pareja.find({where : {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codGrupo : req.param('codGrupo')}});

    var records = [];

    for(var i = 0; i < parejas.length; i++){
      var usr1 = await Usuario.findOne({where : {codUsr : parejas[i].codUsr1}});
      var usr2 = await Usuario.findOne({where : {codUsr : parejas[i].codUsr2}});

      records.push(usr1.nombre);
      records.push(usr1.apellidos);
      records.push(usr2.nombre);
      records.push(usr2.apellidos);
      records.push(parejas[i].puntuacionLigaRegular);
    }

    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      records: records,
      session : req.session
    });
  },
  enfrentamientos: async function(req, res) {
    var records = await ParejasEnfrentamiento.find(
      {where : {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codGrupo : req.param('codGrupo')},
        sort: 'codEnfrentamiento ASC'
      });


    var nombres = [];

    for(var i = 0; i < records.length; i++){
      var capitan = await Usuario.findOne({where : {codUsr : records[i].capitan}});
      var jugador = await Usuario.findOne({where : {codUsr : records[i].jugador}});

      nombres.push(capitan.nombre);
      nombres.push(capitan.apellidos);
      nombres.push(jugador.nombre);
      nombres.push(jugador.apellidos);
    }

    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      records: records,
      nombres: nombres,
      session : req.session
    });
  },

  edit: async function(req, res) {
    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      capitan1 : req.param('capitan1'),
      jugador1 : req.param('jugador1'),
      sets1 : req.param('sets1'),
      capitan2 : req.param('capitan2'),
      jugador2 : req.param('jugador2'),
      sets2 : req.param('sets2'),
      records: await ParejasEnfrentamiento.find({where : {codEnfrentamiento: req.param('codEnfrentamiento')}}),
      session : req.session
    });
  },

  confirmEdit: async function(req, res) {
    var resultado1 = parseInt(req.param('resultado1'));
    var resultado2 = parseInt(req.param('resultado2'));

    if((resultado1 + resultado2)<=3){
      console.log("Entramos a editar con puntuacion correcta");
      //Añadimos los sets al partido de la pareja 1
      var param1 = {
        codEnfrentamiento: req.param('codEnfrentamiento'),
        codCamp: req.param('codCamp'),
        codCategoria: req.param('codCategoria'),
        codNivel: req.param('codNivel'),
        codGrupo: req.param('codGrupo'),
        capitan: req.param('capitan1'),
        jugador: req.param('jugador1'),
      };
      await ParejasEnfrentamiento.updateOne(param1).set({resultado : req.param('resultado1')}).exec((_err, _res) =>{});
      //Añadimos los sets al partido de la pareja 2
      var param2 = {
        codEnfrentamiento: req.param('codEnfrentamiento'),
        codCamp: req.param('codCamp'),
        codCategoria: req.param('codCategoria'),
        codNivel: req.param('codNivel'),
        codGrupo: req.param('codGrupo'),
        capitan: req.param('capitan2'),
        jugador: req.param('jugador2'),
      };
      await ParejasEnfrentamiento.updateOne(param2).set({resultado : req.param('resultado2')}).exec((_err, _res) =>{});
      //Añadimos putuacion a la pareja 1
      var param3 = {
        codCamp: req.param('codCamp'),
        codCategoria: req.param('codCategoria'),
        codNivel: req.param('codNivel'),
        codGrupo: req.param('codGrupo'),
        codUsr1: req.param('capitan1'),
        codUsr2: req.param('jugador1'),
      };
      var param4 = {
        codCamp: req.param('codCamp'),
        codCategoria: req.param('codCategoria'),
        codNivel: req.param('codNivel'),
        codGrupo: req.param('codGrupo'),
        codUsr1: req.param('capitan2'),
        codUsr2: req.param('jugador2'),
      };
      if(req.param('resultado1') > req.param('resultado2')){
        var puntuacion = await Pareja.findOne(param3);

        await Pareja.updateOne(param3).set({puntuacionLigaRegular : (puntuacion.puntuacionLigaRegular+3)}).exec((_err, _res) =>{
          res.redirect('/gestionarcampeonatos');
        });
      }else{
        var puntuacion = await Pareja.findOne(param4);

        await Pareja.updateOne(param4).set({puntuacionLigaRegular : (puntuacion.puntuacionLigaRegular+3)}).exec((_err, _res) =>{
          res.redirect('/gestionarcampeonatos');
        });
      }
    }else{
      //Resultado no valido, indicarlo con una redireccion o algo.
      console.log("puntuacion NO correcta");
      res.redirect('/gestionarcampeonatos');
    }
  },
  generarLigaRegular: async function(req, res) {

    var nUsuarios = await Pareja.count({
      codCamp : req.param('codCamp'),
      codCategoria : req.param('codCategoria'),
      codNivel : req.param('codNivel'),
      codGrupo : req.param('codGrupo')
    });
    //Para ver el numero de parejas y ver si se aigna todo bien, quitarlo despues
    console.log('Numero de parejas');
    console.log(nUsuarios);

    var grupos = await sails.helpers.calcularNumGruposLiga(nUsuarios);
    var aux = [];
    var aplanao = await sails.helpers.aplanarArray(aux, grupos);

    //Miramosque aplanao crese bien los grupos
    console.log(aplanao);

    /**Si no hay gente suficiente para un grupo:
     * -Notifica a las parejas: No habia gente suficiente
     * -Elimina las parejas.
     * -Elimina el grupo y nivel del campeonato.
     */
    if(aplanao[0] == 0){
      /**Notificamos a las parejas y las vamos eliminando*/
      var records = await Pareja.find({where : {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codGrupo : req.param('codGrupo')}});

      for(var parejas=0; parejas<records.length; parejas++){
        /**Notificamos a la pareja */
        var param = {
          receptor: records[parejas].codUsr1,
          emisor: '0',
          asunto: 'Participacion Torneo',
          texto : 'No hay gente suficiente pra jugar el nivel y categoria del campeonato',
          aux1: req.param('codCamp'),
          aux2: req.param('codCategoria'),
          aux3: req.param('codNivel'),
          /*tipo: 0 pareja, 1 partido, 2 solo lectura */
          tipo: '2',
          /*aux, en pareja tiene codGrupo */
          aux4: '0',
          visto : 'false',
        };

        await Notificaciones.create(param);

        /**Borramos la pareja */  
        param = {
          codUsr1: records[parejas].codUsr1,
          codUsr2: records[parejas].codUsr2,
          codCamp: req.param('codCamp'),
          codCategoria: req.param('codCategoria'),
          codNivel: req.param('codNivel'),
          codGrupo: req.param('codGrupo'),
        };
        await Pareja.destroyOne(param);
      }
      /**Borramos el grupo y el nivel */
      var param2 = {
        codCamp: req.param('codCamp'),
        codCategoria: req.param('codCategoria'),
        codNivel: req.param('codNivel'),
        codGrupo: req.param('codGrupo'),
      };
      await Grupo.destroyOne(param2);
      param2 = {
        codCamp: req.param('codCamp'),
        codCategoria: req.param('codCategoria'),
        codNivel: req.param('codNivel'),
      };
      await Nivel.destroyOne(param2).exec((_err, _res) => {
        res.redirect('/gestionarLigaRegular/categorias?codCamp='+req.param('codCamp'));
      });
    }else{
      //Si entramos aqui hay gente suficiente para crear grupos y enfrentamientos
      //Crear tantos grupos como posiciones tenga el array
      //Empezamos en el 2 para ir creando a partir del 1 que ya esta, para eso a length le sumamos 1
      //Ej aplanao length 2, creamos dos grupos pero el 1 ya esta, empezamos en el dos directamente
      //Y la siguienteiteracion no la hace, porque 2+1==aplanao.length+1
      for(var i=2; i<(aplanao.length + 1); i++){
        var newGrupo = {
          codCamp : req.param('codCamp'),
          codCategoria : req.param('codCategoria'),
          codNivel : req.param('codNivel'),
          codGrupo : i
        }

        await Grupo.create(newGrupo);
      }

      //Repartimos a las parejas en los distintos grupos en funcion de aplanao.
      //Obtenemos la lista de todas las parejas
      var parejas = await Pareja.find({where : {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codGrupo : req.param('codGrupo')}});
      var parejasTotalesRepartidas = 0; //Cuantas repartimos, por si hay que notificar y borrar alguna

      //Nos movemos por las distintas posiciones de aplanao, que son los grupos
      for(var i= 0; i<aplanao.length; i++){
        //Para cada grupo asignamos al numero de parejas que hay en aplanao[i] a su grupo.
        for(var y = 0; y<aplanao[i];y++){
          var param = {
            codCamp : req.param('codCamp'),
            codCategoria : req.param('codCategoria'),
            codNivel : req.param('codNivel'),
            codUsr1 : parejas[parejasTotalesRepartidas].codUsr1,
            codUsr2 : parejas[parejasTotalesRepartidas].codUsr2
          }
          await Pareja.updateOne(param).set({codGrupo : (i+1)});

          parejasTotalesRepartidas++;
        }
      }
      //Para orientarse y pruebas, miramos cuantas repartio y el total, hay que notificr y borrar las que queden
      console.log('Parejas repartidas');
      console.log(parejasTotalesRepartidas);
      console.log('Parejas totales');
      console.log(parejas.length);
      //Al acabar de repartir, si quedan más parejas, hay que notificarles que no hay sitio para ellas
      //Ademas hay que borrarlas para no tenerlas en cuenta al crear enfrentamientos
      for(var i = parejasTotalesRepartidas; i<parejas.length; i++){
        var param = {
          receptor: parejas[i].codUsr1,
          emisor: '0',
          asunto: 'Participacion Torneo',
          texto : 'No hay sitio para que participes en el torneo',
          aux1: req.param('codCamp'),
          aux2: req.param('codCategoria'),
          aux3: req.param('codNivel'),
          /*tipo: 0 pareja, 1 partido, 2 solo lectura */
          tipo: '2',
          /*aux, en pareja tiene codGrupo */
          aux4: '0',
          visto : 'false',
        };

        await Notificaciones.create(param);

        /**Borramos la pareja */
        
        param = {
          codUsr1: parejas[i].codUsr1,
          codUsr2: parejas[i].codUsr2,
          codCamp: req.param('codCamp'),
          codCategoria: req.param('codCategoria'),
          codNivel: req.param('codNivel'),
          codGrupo: req.param('codGrupo'),
        };
        await Pareja.destroyOne(param);
      }

      //Una vez llegamos aqui tenemos los grupos y cada uno con su pareja, hay que crear en cada grupo:
      //-Enfrentamiento
      //-ParejasEnfrentamiento de cda enfrentamiento.

      for(var i = 0; i<aplanao.length; i++){
        //Accedemos a las parejas de cada grupo
        var parejasDelGrupo = await Pareja.find({where : {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codGrupo : (i+1)}});
        //EL primer bucle recorre desde la primera a la penultima pareja
        for(var x = 0; x< (parejasDelGrupo.length - 1); x++){
          //El segundo bucle recorre desde la pareja siguiente a la ultima creando enfrentamientos contra todas las anteriores
          for(var y = (x+1); y < parejasDelGrupo.length; y++){
            //Creamos el enfrentamiento
            var param = {
              codCamp: req.param('codCamp'),
              codCategoria: req.param('codCategoria'),
              codNivel: req.param('codNivel'),
              fecha: '0-0-0',
              hora: 0,
              fase: 0
            }
            var enfrentamiento = await Enfrentamiento.create(param).fetch();

            //Para cada enfrentamiento creamos sus dos tuplas en Parejas Enfrentamiento
            //Esta es la pareja del bucle principal, se mantiene fija hasta que el segundo bucle le asigne enfrentamientos con l resto
            var parejaBase = {
              codEnfrentamiento : enfrentamiento.codEnfrentamiento,
              codCamp: req.param('codCamp'),
              codCategoria: req.param('codCategoria'),
              codNivel: req.param('codNivel'),
              codGrupo: (i+1),
              capitan: parejasDelGrupo[x].codUsr1,
              jugador: parejasDelGrupo[x].codUsr2
            }
            await ParejasEnfrentamiento.create(parejaBase);
            //Esta pareja depende del segundo bucle, son las que se estan asignando a la primera.
            var parejaEnMovimiento = {
              codEnfrentamiento : enfrentamiento.codEnfrentamiento,
              codCamp: req.param('codCamp'),
              codCategoria: req.param('codCategoria'),
              codNivel: req.param('codNivel'),
              codGrupo: (i+1),
              capitan: parejasDelGrupo[y].codUsr1,
              jugador: parejasDelGrupo[y].codUsr2
            }
            await ParejasEnfrentamiento.create(parejaEnMovimiento);
          }
          //Aqui acaba el segundo bucle, porejemplo, en la primera iteracion debio crear enfrentamientos
          //de la pareja en parejasDelGrupo[0] contra el resto de integrantes del array
        }
        //Aqui acaba de crear los enfrentamientosde todas las prejas de ese grupo y pasa al siguiente grupo
      }
    }//Cierra el else
    res.redirect('/perfil');
  },
  generarPlayOffs: async function(req, res) {
    //Sacamos ordenadas las parejas del grupo
    var parejasOrdenadas = await Pareja.find(
      {where : {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codGrupo : req.param('codGrupo')},
        sort: 'puntuacionLigaRegular DESC'
      }
    );

    //Creamos los 4 enfrentamientos con sus tuplas en parejas enfrentamiento
    //Enfrentamiento 1º vs 8º
    var enfrentamiento;

    var param = {
      codCamp: req.param('codCamp'),
      codCategoria: req.param('codCategoria'),
      codNivel: req.param('codNivel'),
      fecha: '0-0-0',
      hora: 0,
      fase: 1
    };

    enfrentamiento = await Enfrentamiento.create(param).fetch();

    //ParejasEnfrentamiento 1º vs 8º
    var param2 = {
      codEnfrentamiento: enfrentamiento.codEnfrentamiento,
      codCamp: req.param('codCamp'),
      codCategoria: req.param('codCategoria'),
      codNivel: req.param('codNivel'),
      codGrupo: req.param('codGrupo'),
      capitan: parejasOrdenadas[0].codUsr1,
      jugador: parejasOrdenadas[0].codUsr2
    };
    await ParejasEnfrentamiento.create(param2);
    param2.capitan = parejasOrdenadas[7].codUsr1;
    param2.jugador = parejasOrdenadas[7].codUsr2;
    await ParejasEnfrentamiento.create(param2);

    //Enfrentamiento 2º vs 7º
    param.fase = 3;
    enfrentamiento = await Enfrentamiento.create(param).fetch();

    //ParejasEnfrentamiento 2º vs 7º
    param2.codEnfrentamiento = enfrentamiento.codEnfrentamiento;
    param2.capitan = parejasOrdenadas[1].codUsr1;
    param2.jugador = parejasOrdenadas[1].codUsr2;
    await ParejasEnfrentamiento.create(param2);

    param2.capitan = parejasOrdenadas[6].codUsr1;
    param2.jugador = parejasOrdenadas[6].codUsr2;
    await ParejasEnfrentamiento.create(param2);

    //Enfrentamiento 3º vs 6º
    param.fase = 4;
    enfrentamiento = await Enfrentamiento.create(param).fetch();

    //ParejasEnfrentamiento 3º vs 6º
    param2.codEnfrentamiento = enfrentamiento.codEnfrentamiento;
    param2.capitan = parejasOrdenadas[2].codUsr1;
    param2.jugador = parejasOrdenadas[2].codUsr2;
    await ParejasEnfrentamiento.create(param2);

    param2.capitan = parejasOrdenadas[5].codUsr1;
    param2.jugador = parejasOrdenadas[5].codUsr2;
    await ParejasEnfrentamiento.create(param2);

    //Enfrentamiento 4º vs 5º
    param.fase = 2;
    enfrentamiento = await Enfrentamiento.create(param).fetch();

    //ParejasEnfrentamiento 4º vs 5º
    param2.codEnfrentamiento = enfrentamiento.codEnfrentamiento;
    param2.capitan = parejasOrdenadas[3].codUsr1;
    param2.jugador = parejasOrdenadas[3].codUsr2;
    await ParejasEnfrentamiento.create(param2);

    param2.capitan = parejasOrdenadas[4].codUsr1;
    param2.jugador = parejasOrdenadas[4].codUsr2;
    await ParejasEnfrentamiento.create(param2);

    res.redirect('/gestionarcampeonatos');
  },

  verPlayOffs: async function(req, res) {
    //Sacamos ordenadas las parejas del grupo
    var parejasOrdenadas = await Pareja.find(
      {where : {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codGrupo : req.param('codGrupo')},
        sort: 'puntuacionLigaRegular DESC'
      }
    );
    
    //Sacamos los enrentamientos con fase 1, 2, 3 y 4 de la bd
    var partidos = [];

    var pareja;
    var nombreUsr1;
    var nombreUsr2;
    
    nombreUsr1 = await sails.helpers.getNombreUsuario(parejasOrdenadas[0].codUsr1);
    nombreUsr2 = await sails.helpers.getNombreUsuario(parejasOrdenadas[0].codUsr2);
    pareja = nombreUsr1 + ' y ' + nombreUsr2;
    partidos.push(pareja);

    nombreUsr1 = await sails.helpers.getNombreUsuario(parejasOrdenadas[7].codUsr1);
    nombreUsr2 = await sails.helpers.getNombreUsuario(parejasOrdenadas[7].codUsr2);
    pareja = nombreUsr1 + ' y ' + nombreUsr2;
    partidos.push(pareja);

    nombreUsr1 = await sails.helpers.getNombreUsuario(parejasOrdenadas[3].codUsr1);
    nombreUsr2 = await sails.helpers.getNombreUsuario(parejasOrdenadas[3].codUsr2);
    pareja = nombreUsr1 + ' y ' + nombreUsr2;
    partidos.push(pareja);

    nombreUsr1 = await sails.helpers.getNombreUsuario(parejasOrdenadas[4].codUsr1);
    nombreUsr2 = await sails.helpers.getNombreUsuario(parejasOrdenadas[4].codUsr2);
    pareja = nombreUsr1 + ' y ' + nombreUsr2;
    partidos.push(pareja);

    nombreUsr1 = await sails.helpers.getNombreUsuario(parejasOrdenadas[1].codUsr1);
    nombreUsr2 = await sails.helpers.getNombreUsuario(parejasOrdenadas[1].codUsr2);
    pareja = nombreUsr1 + ' y ' + nombreUsr2;
    partidos.push(pareja);

    nombreUsr1 = await sails.helpers.getNombreUsuario(parejasOrdenadas[6].codUsr1);
    nombreUsr2 = await sails.helpers.getNombreUsuario(parejasOrdenadas[6].codUsr2);
    pareja = nombreUsr1 + ' y ' + nombreUsr2;
    partidos.push(pareja);

    nombreUsr1 = await sails.helpers.getNombreUsuario(parejasOrdenadas[2].codUsr1);
    nombreUsr2 = await sails.helpers.getNombreUsuario(parejasOrdenadas[2].codUsr2);
    pareja = nombreUsr1 + ' y ' + nombreUsr2;
    partidos.push(pareja);

    nombreUsr1 = await sails.helpers.getNombreUsuario(parejasOrdenadas[5].codUsr1);
    nombreUsr2 = await sails.helpers.getNombreUsuario(parejasOrdenadas[5].codUsr2);
    pareja = nombreUsr1 + ' y ' + nombreUsr2;
    partidos.push(pareja);

    //Metemos interrogantes para la primera visualizacion
    partidos.push('¿?');
    partidos.push('¿?');
    partidos.push('¿?');
    partidos.push('¿?');
    partidos.push('¿?');
    partidos.push('¿?');
    partidos.push('¿?');

    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      partidos: partidos,
      session: req.session
    });
  },
};

