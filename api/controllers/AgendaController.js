/**
 * AgendaController
 *
 * @description :: Permite a un entrenador visualizar su calendario para las próximas semanas
 */

module.exports = {

  show: async function(req, res) {

    var matriz = []; // Contiene los dias con las reservas asociadas
    var semana = []; //Guardamos las fechas en la matriz para que al darle a siguiente se refleje en la vista

    var incremento; //Variable para l boton de siguiente

    incremento= parseInt(req.param('incremento'));
    //Recorremos la semana
    for(i = 0; i < 7; i++){
      //Inicializamos fecha sumandole i + incremente para avanzar 7 dias desde la fecha presente
      var fecha = new Date();
      fecha.setDate(fecha.getDate() + i + incremento);
      semana.push(fecha.toLocaleDateString());

      //Recorremos las horas de cada dia de la semana
      for(j = 0; j < 8; j++){
        //Para cada hora de cada día comprobamos si hay una reserva asociada entre todas
        var nReservas = await Reserva.count({
          fecha : fecha.toLocaleDateString(),
          hora : j
        });
        if (nReservas === 0){ // Si no hay reservas ponemos false
          matriz.push(false);
        }
        else {
          //Si hay reserva, miramos si es escueldadeportiva o claseparticular
          var reserva = await Reserva.findOne({
            fecha : fecha.toLocaleDateString(),
            hora : j
          });

          var hay = await ClaseParticular.count({codReserva : reserva.codReserva});

          if(hay == 0){
            matriz.push('Escuela deportiva en la pista: ' + reserva.codPista);
          }else{
            matriz.push('Clase particular en la pista: ' + reserva.codPista);
          }
        }
      }
    }

    res.view({
      semana: semana,
      incremento: incremento + 7,
      matriz: matriz,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
};
