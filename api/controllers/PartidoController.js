/**
 * PartidoController
 *
 * @description :: Permite promocionar un partido, modificarlo, eliminarlo y mostrar todos los partidos en promoción.
 */

module.exports = {
  insert: async function(req, res) {
    res.view({
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  confirmInsert: async function(req, res) {
    var partido = {
      fecha: req.param('fecha'),
      hora: req.param('hora'),
      promotor: req.session.codUsr
    };
    await Partido.create(partido).exec((_err, _res) => {
      res.redirect('/promocionarpartido');
    });
  },

  show: async function(req, res) {
    var records;
    if(req.session.tipo === 2){
      records = await Partido.find({
        where: {
          cancelado : false
        },
        sort: 'codPartido DESC'
      });
    }
    else {
      records = await Partido.find({
        where: {
          cancelado : false,
          promotor  : req.session.codUsr,
          fecha: {'>=': new Date().toLocaleDateString()}
        },
        sort: 'codPartido DESC'
      });
    }
    for(i = 0; i < records.length; i++){
      records[i].codReserva = await sails.helpers.getVistaReserva(records[i].codReserva);
      records[i].fecha = await sails.helpers.formatearFechaHora(records[i].fecha, records[i].hora);
      records[i].promotor = await sails.helpers.getNombreUsuario(records[i].promotor);
    }
    res.view({
      records: records,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  delete: async function(req, res) {
    var partido = await Partido.findOne({codPartido : req.param('codPartido')});
    if(req.session.tipo === 2 || req.session.codUsr === partido.promotor){
      res.view({
        codPartido: partido.codPartido,
        notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
      });
    }
    else{
      return res.forbidden();
    }
  },

  confirmDelete: async function(req, res) {
    var partido = await Partido.findOne({codPartido : req.param('codPartido')});
    if(req.session.tipo === 2 || req.session.codUsr === partido.promotor){
      await Partido.destroyOne({codPartido: partido.codPartido});
      await Reserva.destroyOne({codReserva: partido.codReserva});
      res.redirect('/promocionarpartido');
    }
    else{
      return res.forbidden();
    }
  },

  edit: async function(req, res) {
    var record = await Partido.findOne({codPartido : req.param('codPartido')});
    if(req.session.tipo === 2 || req.session.codUsr === record.promotor){
      record.fecha = await sails.helpers.dateToImput(record.fecha);
      res.view({
        record: record,
        session : req.session,
        notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
      });
    }
    else{
      return res.forbidden();
    }
  },

  confirmEdit: async function(req, res) {
    var record = await Partido.findOne({codPartido : req.param('codPartido')});
    if(req.session.tipo === 2 || req.session.codUsr === record.promotor){
      var partido = {
        codPartido: req.param('codPartido'),
        fecha: req.param('fecha'),
        hora: req.param('hora')
      };
      await Partido.updateOne({codPartido : partido.codPartido})
        .set({
          fecha: partido.fecha,
          hora: partido.hora
        }).exec((_err, _res) =>{
          res.redirect('/promocionarpartido');
        });
    }
    else{
      return res.forbidden();
    }
  }
};
