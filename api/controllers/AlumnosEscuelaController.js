/**
 * AlumnosEscuelaController
 *
 * @description :: Permite inscribirse a una escuela, mostrar las escuelas activas y eliminarlas.
 */

module.exports = {
  show: async function(req, res) {
    var records = await EscuelaDeportiva.find();
    res.view({
      tipo : req.session.tipo,
      records: records,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  showInscrito: async function(req, res) {
    var records = await AlumnosEscuela.find({alumno: req.session.codUsr});
    var nombres = [];
    for(i=0; i<records.length;i++){
      nombres.push(await sails.helpers.getNombreEscuela(records[i].escuela));
    }
    res.view({
      tipo : req.session.tipo,
      records: records,
      nombres: nombres,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  insert: async function(req, res) {
    cupoMax = req.param('cupoMax');
    escuela = req.param('codEsc');
    var precio = await Precios.findOne({codPrecio: '4'}); // Consulta el precio del servicio
    var precioFinal = precio.precio;    
    inscritos = await AlumnosEscuela.count({escuela: req.param('codEsc')});
    if(inscritos >= cupoMax){
      aceptar = false;
    }
    else{
      aceptar = true;
    }
    res.view({
        session : req.session,
        precio : precioFinal,
        aceptar : aceptar,
        codEsc : escuela,
        notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
      });
  },
  insertInscripcion: async function(req, res) {
    var param = {
      escuela: req.param('codEsc'),
      alumno: req.session.codUsr,
    };
    await AlumnosEscuela.create(param).exec((_err, _res) => {
      res.redirect('/misescuelas');
    });
  },
  
  delete: async function(req, res) {
    escuela = req.param('escuela');
    nombre = await EscuelaDeportiva.findOne({codEsc : escuela});
    nombreEscuela = nombre.nombre;
    res.view({
      escuela: escuela,
      nombre: nombreEscuela,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  confirmDelete: async function(req, res) {
    var escuela = req.param('escuela');
    await AlumnosEscuela.destroyOne(
        {
          escuela: escuela,
          alumno: req.session.codUsr,
        }).exec((_err, _res) => {
      res.redirect('/misescuelas');
    });
  },
};