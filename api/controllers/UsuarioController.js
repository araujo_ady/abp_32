/**
 * UserController
 *
 * @description :: Permite crear, editar mostrar y eliminar usuarios;
 * modificar su contraseña y su suscripción como socio; visualizar su perfil;
 * y realziar un login en la aplicación.
 */

module.exports = {
  insert: async function(req, res) {
    res.view({
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  confirmInsert: async function(req, res) {
    var param = {
      correo: req.param('correo'),
      password: req.param('password'),
      nombre: req.param('nombre'),
      apellidos: req.param('apellidos'),
      fechaNac: new Date(req.param('fechaNac')).toLocaleDateString(),
      genero: req.param('genero')
    };
    await Usuario.create(param).exec((_err, _nuevoUsuario) => {
      res.redirect('/gestionarusuarios');
    });
  },
  show: async function(req, res) {
    var records = await Usuario.find();
    for(i = 0; i < records.length; i++){
      records[i].fechaNac = await sails.helpers.formatearFecha(records[i].fechaNac);
    }
    res.view({
      records: records,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  delete: async function(req, res) {
    codUsr = req.param('codUsr');
    res.view({
      codUsr: codUsr,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  confirmDelete: async function(req, res) {
    var codUsr = req.param('codUsr');
    await Usuario.destroyOne({codUsr: codUsr}).exec((_err, _res) => {
      res.redirect('/gestionarusuarios');
    });
  },
  edit: async function(req, res) {
    var record = await Usuario.findOne({codUsr : req.param('codUsr')});
    record.fechaNac = await sails.helpers.dateToImput(record.fechaNac);
    res.view({
      record: record,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  confirmEdit: async function(req, res) {
    var param = {
      correo: req.param('correo'),
      nombre: req.param('nombre'),
      apellidos: req.param('apellidos'),
      fechaNac: req.param('fechaNac'),
      genero: req.param('genero')
    };
    await Usuario.updateOne({codUsr : req.param('codUsr')})
    .set(param).exec((_err, _res) =>{
      res.redirect('/gestionarusuarios');
    });
  },
  generateHashNewPassword: async function(req, res) {
    await Usuario.updateOne({codUsr: req.session.codUsr})
      .set({hash : req.session.codUsr + new Date().toDateString()}); // Genera un Hash con codUsr y la fecha actual
    user = await Usuario.findOne({codUsr: req.session.codUsr});
    var email = {
      receptor: user.correo,
      asunto: 'Cambio de contraseña',
      texto: 'Se ha solicitado un cambio de la contraseña de su cuenta.\
              Para cambiar su contraseña, pulse en el siguiente enlace: ' +
              sails.config.custom.url + 'confirmarCambioPassword?correo=' + req.session.correo + '&hash=' + user.hash +
              ' Si no has solicitado cambio de contraseña, por favor, ignora este correo.',
    };
    await sails.helpers.sendMail(email.receptor, email.asunto, email.texto);
    res.view({
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  recuperarPassword: async function(req, res) {
    // No se puede recuperar la contraseña si ya estás logeado
    if (typeof req.session.codUsr === 'undefined') {
      res.view();
    }
    else{
      res.redirect('/');
    }
  },
  confirmRecuperarPassword: async function(req, res) {
    // No se puede recuperar la contraseña si ya estás logeado
    if (typeof req.session.codUsr === 'undefined') {
      var user = await Usuario.findOne({correo: req.param('correo')});
      if(user){ // Se comprueba que exista un usuario con ese correo
        await Usuario.updateOne({codUsr: user.codUsr})
          .set({hash : user.codUsr + new Date().toDateString()}); // Genera un Hash con codUsr y la fecha actual
        user = await Usuario.findOne({codUsr: user.codUsr});
        var email = {
          receptor: user.correo,
          asunto: 'Cambio de contraseña',
          texto: 'Se ha solicitado un cambio de la contraseña de su cuenta.\
                Para cambiar su contraseña, pulse en el siguiente enlace: ' +
                sails.config.custom.url + 'confirmarCambioPassword?correo=' + user.correo + '&hash=' + user.hash +
                ' Si no has solicitado cambio de contraseña, por favor, ignora este correo.',
        };
        await sails.helpers.sendMail(email.receptor, email.asunto, email.texto);
        res.view({
          mensaje : 'Se ha enviado un mensaje a tu correo electrónico con los pasos que seguir a continuación.'
        });
      }
      else{
        res.view({
          mensaje : 'No hemos encontrado ninguna cuenta con ese correo electrónico. Por favor, revisa tus datos y vuelve a intentarlo.'
        });
      }
    }
    else{
      res.redirect('/');
    }
  },
  cambiarPassword: async function(req, res) {
    var correo = req.param('correo');
    var hash   = req.param('hash');
    user = await Usuario.findOne({correo : correo});
    // Por algún motivo, los + procedentes del GET se cortan, lo hacemos también en la variable en DB
    user.hash = user.hash.replace(/\+/g, ' ');
    if(user.hash === hash){
      res.view({
        record: user,
        hash: hash
      });
    }
    else{
      res.forbidden();
    }
  },
  confirmCambiarPassword: async function(req, res) {
    await Usuario.updateOne({ codUsr : req.param('codUsr') })
      .set({
        password : req.param('password'),
        hash: null
      });
    res.redirect('/logout');
  },
  cambiarAvatar: async function(req, res) {
    req.file('avatar').upload({
      dirname: '../public/images/' + req.session.codUsr,
      maxBytes: 3000000,
      saveAs: 'avatar.png'
    },
      (err, _uploadedFiles) => {
        if (err) {
          return res.serverError(err);
        }
        Usuario.updateOne(
        {
          codUsr:req.session.codUsr
        }).set({
          avatar: '/images/' + req.session.codUsr + '/avatar.png'
        }).exec((err) => {
          if (err){
            return res.serverError(err);
          }
          req.session.avatar = '/images/' + req.session.codUsr + '/avatar.png';
          res.redirect('/');
        });
      });
  },
  altaSocio: async function(req, res) {
    var record = await Usuario.findOne({codUsr : req.session.codUsr});
    if(record.esSocio === false){ // Solo tiene sentido que lo haga si es socio
      res.view({
        record: record,
        session : req.session,
        notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
      });
    }
    else{
      res.redirect('/gestionarusuarios/bajaSocio');
    }
  },
  confirmAltaSocio: async function(req, res) {
    await req.file('banco').upload({
      dirname: '../public/recipes/' + req.session.codUsr,
      maxBytes: 3000000,
      saveAs: 'recipe.pdf'
    },
    (err, _uploadedFiles) => {
      if (err) {
        return res.serverError(err);
      }
      else{
        Usuario.updateOne(
          {
            codUsr: req.session.codUsr
          }).set({
          banco: '/recipes/' + req.session.codUsr + '/recipe.pdf'
        }).exec((err) => {
          if (err){
            return res.serverError(err);
          }
        });
      }
    });
    var admins = await Usuario.find({tipo : '2'});
    var mensaje = {
      receptor: 0,
      emisor: req.session.codUsr,
      tipo: 4,
      asunto: 'Nueva alta pendiente de validar',
      texto: 'El usuario ' + req.session.nombre + ' ' + req.session.apellidos + 'ha solicitado el alta como socio.'
    };
    var email = {
      receptor: 0,
      asunto: 'Tienes una nueva alta por validar',
      texto: 'Un usuario ha solicitado su alta como socio. Accede a la aplicación para validar sus datos bancarios.',
    };
    for(i = 0; i < admins.length; i++){
      mensaje.receptor = admins[i].codUsr;
      email.receptor = admins[i].correo;
      await Notificaciones.create(mensaje);
      await sails.helpers.sendMail(email.receptor, email.asunto, email.texto);
    }
    res.redirect('/');
  },
  validarSocio: async function(req, res) {
    var not = await Notificaciones.findOne({codNot : req.param('codNot')});
    var user = await Usuario.findOne({codUsr : req.param('codUsr')});
    await Notificaciones.updateOne({codNot : req.param('codNot')})
      .set({
        visto: true,
        tipo : '2',
        texto: not.texto + ' Esta petición ha sido aceptada.'
      });
    await Usuario.updateOne({codUsr : req.param('codUsr')}).set({esSocio : '1'});
    var mensaje = {
      receptor: req.param('codUsr'),
      emisor: req.session.codUsr,
      tipo: 2,
      asunto: '¡Bienvenido!',
      texto: 'Su alta ha sido procesada y ya es socio de nuestro club. ¡Enhorabuena!'
    };
    var email = {
      receptor: user.correo,
      asunto: 'Tienes una nueva notificación',
      texto: 'Parece que tienes nuevas notificaciones sin leer. Entra a la aplicación para consultarlas.',
    };
    await Notificaciones.create(mensaje);
    await sails.helpers.sendMail(email.receptor, email.asunto, email.texto);
    res.redirect('/gestionarNotificaciones');
  },
  cancelarValidacion: async function(req, res) {
    var user = await Usuario.findOne({codUsr : req.param('codUsr')});
    var not = await Notificaciones.findOne({codNot : req.param('codNot')});
    await Usuario.updateOne({codUsr : req.param('codUsr')}).set({banco : ''});
    await Notificaciones.updateOne({codNot : req.param('codNot')})
      .set({
        visto: true,
        tipo : '2',
        texto: not.texto + ' Esta petición ha sido rechazada.'
      });
    var mensaje = {
      receptor: req.param('codUsr'),
      emisor: req.session.codUsr,
      tipo: 2,
      asunto: 'Alta cancelada',
      texto: 'No hemos podido darle de alta como socio ya que no se ha podido verificar la autenticidad de sus datos bancarios. Por favor, inténtelo de nuevo.'
    };
    var email = {
      receptor: user.correo,
      asunto: 'Tienes una nueva notificación',
      texto: 'Parece que tienes nuevas notificaciones sin leer. Entra a la aplicación para consultarlas.',
    };
    await Notificaciones.create(mensaje);
    await sails.helpers.sendMail(email.receptor, email.asunto, email.texto);
    res.redirect('/gestionarNotificaciones');
  },
  bajaSocio: async function(req, res) {
    var record = await Usuario.findOne({codUsr : req.session.codUsr});
    res.view({
      record: record,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  confirmBajaSocio: async function(req, res) {
    var user = await Usuario.findOne({codUsr : req.param('codUsr')});
    if((req.session.codUsr === parseInt(req.param('codUsr'))) || req.session.tipo === 2){ // Solo accesible por el usuario y el propio administrador
      await Usuario.updateOne({codUsr : req.param('codUsr')}).set(
        {
          esSocio: req.param('esSocio'),
          banco: ''
        }
      );
      if(req.session.tipo === 2){ // Notificar si la baja la hace un administrador
        var mensaje = {
          receptor: req.param('codUsr'),
          emisor: req.session.codUsr,
          tipo: 2,
          asunto: 'Membresía suspendida',
          texto: 'Parece que ha habido algún problema en los pagos y un administrador te ha suspendido como socio. Por favor, revisa tus datos bancarios.'
        };
        var email = {
          receptor: user.correo,
          asunto: 'Tienes una nueva notificación',
          texto: 'Parece que tienes nuevas notificaciones sin leer. Entra a la aplicación para consultarlas.',
        };
        await Notificaciones.create(mensaje);
        await sails.helpers.sendMail(email.receptor, email.asunto, email.texto);
      }
      res.redirect('/');
    }
    else{
      res.forbidden();
    }
  },
  perfil: async function(req,res) {
    var records = await Usuario.findOne({codUsr : req.session.codUsr});
    records.fechaNac = await sails.helpers.formatearFecha(records.fechaNac);
    res.view({
      records: records,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  login: async function(req, res) {
    if(typeof req.session.codUsr === 'undefined'){
      res.view();
    }
    else {
      res.redirect('/perfil');
    }
  },
  auth: async function(req, res) {
    if(typeof req.session.codUsr === 'undefined'){
      var correo   = req.param('correo');
      var password = req.param('password');
      await Usuario.findOne({correo : correo}).decrypt().exec((_err, record) => {
        if(!record){
          res.view({
            error: 'Error: No existe el usuario con el correo especificado.'
          });
        }
        else if((password === record.password)){
          req.session.codUsr = record.codUsr;
          req.session.correo = record.correo;
          req.session.nombre = record.nombre;
          req.session.apellidos = record.apellidos;
          req.session.fechaNac = record.fechaNac;
          req.session.genero = record.genero;
          req.session.esSocio = record.esSocio;
          req.session.tipo = record.tipo;
          req.session.avatar = record.avatar;

          res.redirect('/');
        }
        else {
          res.view({
            error: 'Error: Correo electrónico o contraseña errónea.'
          });
        }
      });
    }
    else{
      res.redirect('/');
    }
  },
  logout: async function(req, res) {
    req.session.destroy((_err) => {
      setTimeout(() => {
        return res.redirect('/');
      }, 1000);
    });
  }
};

