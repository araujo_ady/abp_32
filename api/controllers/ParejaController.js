/**
 * ParejaController
 *
 * @description :: Permite inscribirse a una pareja, mostrar las parejas activas y eliminarlas.
 */

module.exports = {
  insert: async function(req, res) {
    //Miramos que el usuario no este inscrito en el mismo torneo en la misma categoria
    var parejaCapi = await Pareja.count({where : {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codUsr1 : req.param('codUsr1')}});
    var parejaJug = await Pareja.count({where : {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codUsr2 : req.param('codUsr1')}});
    var precio = await Precios.find({codPrecio: 3});

    if((parejaCapi+parejaJug)!=0){
      res.redirect('/perfil');
    }else{
      //Miramos que su pareja sea del genero de la categoria
      //Si es mixto tiene que ser del otro genero,sino del mismo
      var usr2 = await Usuario.findOne({where : {correo : req.param('correo')}});

      if(((req.param('genero') == 'mixta') && (usr2.genero != req.session.genero)) || ((usr2.genero == req.session.genero) && (req.param('genero') != 'mixta'))){
        //Miramos que su pareja no este inscrita en esa categoria con otra persona
        var yaEsCapi = await Pareja.count({where : {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codUsr1 : usr2.codUsr}});
        var yaEsJug = await Pareja.count({where : {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codUsr2 : usr2.codUsr}});

        if((yaEsCapi+yaEsJug)!=0){
          res.redirect('/perfil');
        }else{
          var param = {
            codCamp: req.param('codCamp'),
            codCategoria: req.param('codCategoria'),
            codNivel: req.param('codNivel'),
            codUsr1: req.param('codUsr1'),
            codUsr2: usr2.codUsr,
            codGrupo: '1', //MIRAR PARA QUE INSERTE EN GRUPO DEPENDIENDO DEL BALANCEO AUTOMATICO
            estaFormada : 'false',
            puntuacionLigaRegular : '0',
          };

          var to = usr2.correo;
          var asunto = 'Pareja torneo';
          var texto = '¿Quieres formar pareja con:' + req.session.nombre + ', en el campeonato? Os costara: ' + precio.precio;
          let info = await sails.helpers.sendMail(to, asunto, texto);

          var to = req.session.correo;
          var asunto = 'Precio';
          var texto = 'Anotarte con una pareja en el torneo os costara: ' + precio.precio;
          let info = await sails.helpers.sendMail(to, asunto, texto);
  
          await Pareja.create(param).exec((_err, _res) => {});
          var param2 = {
            receptor: usr2.codUsr,
            emisor: req.param('codUsr1'),
            texto : '¿Quieres formar pareja con: ' + req.session.nombre + ' ' + req.session.apellidos + ', en el campeonato? Os costara: ' + precio.precio,
            aux1: req.param('codCamp'),
            aux2: req.param('codCategoria'),
            aux3: req.param('codNivel'),
            asunto: 'Pareja Campeonato',
            /*tipo: 0 pareja, 1 partido, 2 sololectura */
            tipo: '0',
            /*aux4, en pareja tiene codGrupo */
            aux4: '1', //MIRAR PARA QUE INSERTE EN GRUPO DEPENDIENDO DEL BALANCEO AUTOMATICO
            visto : 'false',
          };
          await Notificaciones.create(param2).exec((_err, _res) => {
            res.redirect('/perfil');
          });
        }
      }else{
        res.redirect('/perfil');
      }
    }
  },
  show: async function(req, res) {

    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      codCamp : req.param('codCamp'),
      nombreCamp : req.param('nombreCamp'),
      codCategoria : req.param('codCategoria'),
      nombreCat : req.param('nombreCat'),
      codNivel : req.param('codNivel'),
      nombreNivel : req.param('nombreNivel'),
      genero : req.param('genero'),
      session : req.session
    });
  },
  delete: async function(req, res) {
      res.view({
        notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
        records: await Pareja.find({where : { codUsr1 : req.session.codUsr}}),
        session : req.session
      });
  },
  confirmDelete: async function(req, res) {
    var param = {
      codUsr1: req.param('codUsr1'),
      codUsr2: req.param('codUsr2'),
      codLiga: req.param('codLiga'),
      codCamp: req.param('codCamp'),
    };
    await Pareja.destroyOne(param).exec((_err, _res) => {
      res.redirect('/gestionarInscripcionCampeonato/borrar');
    });
  }
};
