/**
 * ContenidoDinamicoController
 *
 * @description :: Creación, modificación, borrado y muestra de contenido dinámico
 */

module.exports = {
  show: async function(req, res) {
    var records = await ContenidoDinamico.find({where: {}, sort: 'codContDin DESC'});
    for(i = 0; i < records.length; i++){
      records[i].fecha = await sails.helpers.formatearFecha(records[i].fecha);
    }
    res.view({
      records: records,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  showDetail: async function(req, res) {
    var record = await ContenidoDinamico.findOne({codContDin: req.param('codContDin')});
    record.fecha = await sails.helpers.formatearFecha(record.fecha);
    res.view({
      record: record,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  showLasts: async function(req, res) {
    var records = await ContenidoDinamico.find({where: {}, limit: 3, sort: 'codContDin DESC'});
    for(i = 0; i < records.length; i++){
      records[i].fecha = await sails.helpers.formatearFecha(records[i].fecha);
    }
    res.view({
      records: records,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  insert: async function(req, res) {
    res.view({
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  confirmInsert: async function(req, res) {
    var record = await ContenidoDinamico.create({
      titulo : req.param('titulo'),
      reseña : req.param('reseña'),
      fecha  : new Date(),
      texto  : req.param('texto'),
      link   : req.param('link')
    }).fetch();
    if(typeof req.param('multimedia') === 'undefined'){
      await req.file('multimedia').upload({
        dirname: '../public/images/',
        maxBytes: 3000000,
        saveAs: record.codContDin + '.jpg'
      },
      (err, _uploadedFiles) => {
        if (err) {
          return res.serverError(err);
        }
        ContenidoDinamico.updateOne(
        {
          codContDin: record.codContDin,
        }).set({
          multimedia: '/images/' + record.codContDin + '.jpg',
        }).exec((err) => {
          if (err){
            return res.serverError(err);
          }
        });
      });
    }
    // Limitado a los 10 primeros para que hotmail no nos banee la cuenta inesperadamente
    var user = await Usuario.find({where: {tipo : '0'}, limit: 10});
    var email = {
      receptor: null,
      asunto: record.titulo,
      texto: record.texto,
    };
    for(i = 0; i < user.length; i++){
      email.receptor = user[i].correo;
      await sails.helpers.sendMail(email.receptor, email.asunto, email.texto);
    }
    res.redirect('/gestionarcontenido');
  },

  delete: async function(req, res) {
    var record = await ContenidoDinamico.findOne({codContDin : req.param('codContDin')});
    res.view({
      record : record,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  confirmDelete: async function(req, res) {
    await ContenidoDinamico.destroyOne({codContDin : req.param('codContDin')});
    res.redirect('/gestionarcontenido');
  },

  edit: async function(req, res) {
    var record = await ContenidoDinamico.findOne({codContDin : req.param('codContDin')});
    res.view({
      record : record,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  confirmEdit: async function(req, res) {
    await ContenidoDinamico.updateOne({
      codContDin : req.param('codContDin')
    }).set({
      titulo: req.param('titulo'),
      reseña: req.param('reseña'),
      texto: req.param('texto'),
      link   : req.param('link')
    });
    if(typeof req.param('multimedia') === 'undefined'){
      await req.file('multimedia').upload({
        dirname: '../public/images/',
        maxBytes: 3000000,
        saveAs: req.param('codContDin') + '.jpg'
      },
      (err, _uploadedFiles) => {
        if (err) {
          return res.serverError(err);
        }
        ContenidoDinamico.updateOne(
          {
            codContDin: req.param('codContDin'),
          }).set({
          multimedia: '/images/' + req.param('codContDin') + '.jpg',
        }).exec((err) => {
          if (err){
            return res.serverError(err);
          }
          res.redirect('/gestionarcontenido');
        });
      });
    }
    else {
      res.redirect('/gestionarcontenido');
    }
  },
};

