/**
 * EscuelaDeportivaController
 *
 * @description :: Permite crear, editar mostrar y eliminar escuelas deportivas.
 */

module.exports = {
  insert: async function(req, res) {
    res.view({
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },

  confirmInsert: async function(req, res) {
    var param = {
      entrenador: req.session.codUsr,
      nombre: req.param('nombre'),
      fechaInicio: new Date(req.param('fechaInicio')),
      fechaFin: new Date(req.param('fechaFin')),
      fecha: new Date(req.param('fecha')),
      hora: req.param('hora'),
      cupoMin: req.param('cupoMin'),
      cupoMax: req.param('cupoMax'),
      numClases: req.param('numClases'),
    };

    await EscuelaDeportiva.create(param).fetch().exec((_err, nuevaEscuelaDeportiva) => {
      res.redirect('/gestionarescueladeportiva');
    });
  },
  show: async function(req, res) {
    var records = await EscuelaDeportiva.find();
    res.view({
      tipo : req.session.tipo,
      records: records,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  delete: async function(req, res) {
    var record = await EscuelaDeportiva.findOne({codEsc : req.param('codEsc')});
    res.view({
      record: record,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  confirmDelete: async function(req, res) {
    var codEsc = req.param('codEsc');
    await EscuelaDeportiva.destroyOne({codEsc: codEsc}).exec((_err, _res) => {
      res.redirect('/gestionarescueladeportiva');
    });
  },
  edit: async function(req, res) {
    var record = await EscuelaDeportiva.findOne({codEsc : req.param('codEsc')});
    res.view({
      record: record,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  confirmEdit: async function(req, res) {
    var param = {
      nombre: req.param('nombre'),
      hora: req.param('hora'),
      fechaInicio: req.param('fechaInicio'),
      fechaFin: req.param('fechaFin'),
      fecha: req.param('fecha'),
      cupoMin: req.param('cupoMin'),
      cupoMax: req.param('cupoMax'),
      numClases: req.param('numClases')
    };
    await EscuelaDeportiva.updateOne({codEsc : req.param('codEsc')})
    .set(param).exec((_err, _res) =>{
      res.redirect('/gestionarescueladeportiva');
    });
  },
  reservar: async function(req, res){
    n = req.param('numClases');
    codEsc = req.param('codEsc');
    codUsr = req.param('entrenador');
    fecha = req.param('fecha');
    hora = req.param('hora');
    cupoMin = req.param('cupoMin');
    var codRes;
    codPista = 1;
    inscritos = await AlumnosEscuela.count({escuela: codEsc});
    if(inscritos < cupoMin){
      res.view({
        session : req.session,
        estado: 'fracaso',
        notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
      });
    }
    else{
      for(i=0; i<n; i++){
        await Reserva.create({
          fecha: fecha,
          hora: hora,
          codUsr: codUsr,
          codPista: 1,
        }).fetch().exec((_err, reservaNueva) => {
          ReservasEscuela.create({
            codEsc: codEsc,
            codReserva: reservaNueva.codReserva,
          }).exec((_err, records) =>{
          });
        });
        fecha = await sails.helpers.calcularProximaSemana(fecha);
      }
      res.view({
        session : req.session,
        estado: 'exito',
        notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
      });
    }
  }
};
