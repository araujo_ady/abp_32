/**
 * EnfrentamientoController
 *
 * @description :: Muestra los enfrentamientos y permite escoger fecha
 */

module.exports = {
  showCampeonatos: async function(req, res) {
    var records = await Pareja.find({
      or : [
        {codUsr1 : req.session.codUsr},
        {codUsr2 : req.session.codUsr}
      ]
    });

    for(var i = 0; i < records.length; i++){
      records[i].nombreCamp = await sails.helpers.getNombreCampeonato(records[i].codCamp);
      records[i].nombreCategoria = await sails.helpers.getNombreCategoria(records[i].codCamp, records[i].codCategoria);
      records[i].nombreNivel = await sails.helpers.getNombreNivel(records[i].codCamp, records[i].codCategoria, records[i].codNivel);
    }
    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      records: records,
      session : req.session
    });
  },
  showEnfrentamientos: async function(req, res) {
    var records = await ParejasEnfrentamiento.find({
      or : [
        {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codGrupo : req.param('codGrupo'), capitan : req.session.codUsr},
        {codCamp : req.param('codCamp'), codCategoria : req.param('codCategoria'), codNivel : req.param('codNivel'), codGrupo : req.param('codGrupo'), jugador : req.session.codUsr}
      ]
    });
    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      nombreCamp : req.param('nombreCamp'),
      nombreCat : req.param('nombreCat'),
      nombreNivel : req.param('nombreNivel'),
      records: records,
      session : req.session
    });

  },
  elegirFecha: async function(req, res) {
    var nPistas = await Pista.count(); // Número de pistas del club
    var diasOcupados = []; // Contiene las horas ocupadas de cada día de la semana
    for(i = 0; i < 7; i++){
      var horasOcupadas = []; // Contiene las horas ocupadas de un día
      var fecha = new Date();
      fecha.setDate(fecha.getDate() + i);
      for(j = 0; j < 8; j++){
        var nReservas = await Reserva.count({
          fecha : fecha.toLocaleDateString(),
          hora : j
        });
        if (nReservas === nPistas){ // Si hay tantas pistas como reservas, no hay pistas disponibles
          horasOcupadas.push(true); // La hora j del día i está ocupada
        }
        else {
          horasOcupadas.push(false); // La hora j del día i está libre
        }
      }
      diasOcupados.push(horasOcupadas);
    }
    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      pistas: await Pista.find(),
      horasDisponibles: diasOcupados,
      codEnfrentamiento : req.param('codEnfrentamiento')
    });
  },
  insert: async function(req, res) {
    var fecha = req.param('fecha');
    var dia = new Date();
    dia.setDate(dia.getDate() + (fecha%10)); // Esto es la fecha
    var hora = parseInt(fecha/10); //La hora de 0 a 7
    var horaBien; //La hora como string
    switch(hora){
      case 0:
        horaBien = ('9', '0', '0');
        break;
      case 1:
        horaBien = ('10', '30', '0');
        break;
      case 2:
        horaBien = ('12', '00', '0');
        break;
      case 3:
        horaBien = ('13', '30', '0');
        break;
      case 4:
        horaBien = ('15', '00', '0');
        break;
      case 5:
        horaBien = ('16', '30', '0');
        break;
      case 6:
        horaBien = ('18', '00', '0');
        break;
      case 7:
        horaBien = ('19', '30', '0');
        break;
    }

    var records = await ParejasEnfrentamiento.findOne({
      codEnfrentamiento : req.param('codEnfrentamiento'),
      capitan: {'!=' : req.session.codUsr},
      jugador: {'!=' : req.session.codUsr},
    });

    await Enfrentamiento.updateOne({codEnfrentamiento : req.param('codEnfrentamiento')}).set({fecha : dia, hora : hora}).exec((_err, _res) =>{});

    var usr2 = await Usuario.findOne({
      codUsr : req.param(records.capitan),
    });

    var to = usr2.correo;
    var asunto = 'Enfrentamiento torneo';
    var texto = '¿Quieres jugar el dia:' + dia + ', a la hora ' + horaBien + '?';

    var param2 = {
      receptor: records.capitan,
      emisor: req.session.codUsr,
      texto : '¿Quieres jugar el dia:' + dia + ', a la hora ' +horaBien + ', contra ' + req.session.nombre + ' ' + req.session.apellidos + ', y su pareja?',
      aux1: records.codCamp,
      aux2: records.codCategoria,
      aux3: records.codNivel,
      asunto: 'Enfrentamiento Campeonato',
      /*tipo: 0 pareja, 1 partido */
      tipo: '1',
      /*aux4, en pareja tiene codGrupo */
      aux4: req.param('codEnfrentamiento'), //MIRAR PARA QUE INSERTE EN GRUPO DEPENDIENDO DEL BALANCEO AUTOMATICO
      visto : 'false',
    };

    await Notificaciones.create(param2);

    res.redirect('/perfil');
  },
};
