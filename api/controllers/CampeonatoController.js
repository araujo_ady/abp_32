/**
 * CampeonatoController
 *
 * @description :: Controla la creación, modificación y eliminación de campeonatos.
 */

module.exports = {
  insert: async function(req, res) {
    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      session : req.session
    });
  },

  confirmInsert: async function(req, res) {
    var newCampeonato = await Campeonato.create({
      nombre: req.param('nombre'),
      fechaInicio: req.param('fechaInicio'),
      fechaFin: req.param('fechaFin')
    }).fetch();
    var categorias = [];
    /* CATEGORÍA MASCULINA */
    var categoria = await Categoria.create({
      codCamp: newCampeonato.codCamp,
      codCategoria: '1',
      nombre: 'masculino'
    }).fetch();
    categorias.push(categoria);
    /* CATEGORÍA FEMENINA */
    categoria = await Categoria.create({
      codCamp: newCampeonato.codCamp,
      codCategoria: '2',
      nombre: 'femenina'
    }).fetch();
    categorias.push(categoria);
    /* CATEGORÍA MIXTA */
    categoria = await Categoria.create({
      codCamp: newCampeonato.codCamp,
      codCategoria: '3',
      nombre: 'mixta'
    }).fetch();
    categorias.push(categoria);
    for(i = 0; i < categorias.length; i++){
      // NIVEL AMATEUR
      var nivel = await Nivel.create({
        codCamp: newCampeonato.codCamp,
        codCategoria: categorias[i].codCategoria,
        codNivel: '1',
        nombre: 'amateur'
      }).fetch();
      await Grupo.create({
        codCamp: newCampeonato.codCamp,
        codCategoria: categorias[i].codCategoria,
        codNivel: nivel.codNivel,
        codGrupo: '1'
      });
      // NIVEL SEMIPROFESIONAL
      nivel = await Nivel.create({
        codCamp: newCampeonato.codCamp,
        codCategoria: categorias[i].codCategoria,
        codNivel: '2',
        nombre: 'Semi-profesional'
      }).fetch();
      await Grupo.create({
        codCamp: newCampeonato.codCamp,
        codCategoria: categorias[i].codCategoria,
        codNivel: nivel.codNivel,
        codGrupo: '1'
      });
      //NIVEL PROFESIONAL
      nivel = await Nivel.create({
        codCamp: newCampeonato.codCamp,
        codCategoria: categorias[i].codCategoria,
        codNivel: '3',
        nombre: 'Profesional'
      }).fetch();
      await Grupo.create({
        codCamp: newCampeonato.codCamp,
        codCategoria: categorias[i].codCategoria,
        codNivel: nivel.codNivel,
        codGrupo: '1'
      });
    }
    res.redirect('/GestionarCampeonatos');
  },

  show: async function(req, res) {
    var records = await Campeonato.find();
    for(i = 0; i < records.length; i++){
      records[i].fechaInicio = await sails.helpers.formatearFecha(records[i].fechaInicio);
      records[i].fechaFin = await sails.helpers.formatearFecha(records[i].fechaFin);
    }
    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      session : req.session,
      records : records
    });
  },

  edit: async function(req, res) {
    var record = await Campeonato.findOne({codCamp : req.param('codCamp')});
    record.fechaInicio = await sails.helpers.dateToImput(record.fechaInicio);
    record.fechaFin = await sails.helpers.dateToImput(record.fechaFin);
    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      record: record,
      session : req.session
    });
  },

  confirmEdit: async function(req, res) { 
    await Campeonato.updateOne({
      codCamp : req.param('codCamp')
    }).set({
      nombre: req.param('nombre'),
      fechaInicio: req.param('fechaInicio'),
      fechaFin: req.param('fechaFin'),
    });
    res.redirect('/gestionarcampeonatos');
  },

  delete: async function(req, res) {
    codCamp = req.param('codCamp');
    nombre = req.param('nombre');
    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      codCamp: codCamp,
      nombre: nombre,
      session: req.session
    });
  },

  confirmDelete: async function(req, res) {
    await Campeonato.destroyOne({codCamp: req.param('codCamp')});
    await Categoria.destroy({codCamp: req.param('codCamp')});
    await Grupo.destroy({codCamp: req.param('codCamp')});
    await Nivel.destroy({codCamp: req.param('codCamp')});
    res.redirect('/gestionarcampeonatos');
  },
};

