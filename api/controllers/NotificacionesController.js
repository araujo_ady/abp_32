/**
 * NotificacionesController
 *
 * @description :: Muestra notificaciones de enfrentameinto y pareja, y permite aceptarlas.
 */

module.exports = {
  insert:  async function(req, res){
    var destinatario = req.param('destinatario');
    res.view({
      destinatario: destinatario,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  confirmInsert: async function(req, res){
    var mensaje = {
      receptor: req.param('receptor'), // email!
      emisor: req.session.codUsr,
      tipo: 2,
      asunto: req.param('asunto'),
      texto: req.param('texto')
    };
    var email = {
      receptor: mensaje.receptor,
      asunto: 'Tienes un nuevo mensaje privado',
      texto: '¡Hola! Tienes un nuevo mensaje. Accede a nuestra aplicación para leerlo.',
    };
    mensaje.receptor = await sails.helpers.getCodUsrFromMail(mensaje.receptor);
    await sails.helpers.sendMail(email.receptor, email.asunto, email.texto);
    await Notificaciones.create(mensaje);
    res.redirect('/gestionarNotificaciones');
  },
  showDetail: async function(req, res) {
    var codNot = req.param('codNot');
    var record = await Notificaciones.findOne({codNot: codNot});
    if(record.receptor !== req.session.codUsr){ // Si la notificación no es para ese usuario
      return res.forbidden();
    }
    if (record.tipo === 2){
      await Notificaciones.updateOne({codNot: codNot}).set({visto: '1'});
    }
    else if(record.tipo === 4){
      var usuario = await Usuario.findOne({codUsr : record.emisor});
      record.banco = usuario.banco;
    }
    record.emisorId = record.emisor;
    record.emisor = await sails.helpers.getNombreUsuario(record.emisor);
    res.view({
      record: record,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  show: async function(req, res) {
    var records = await Notificaciones.find({
      receptor : req.session.codUsr
    });
    for(i = 0; i < records.length; i++){
      records[i].emisor = await sails.helpers.getNombreUsuario(records[i].emisor);
      records[i].receptor = await sails.helpers.getNombreUsuario(records[i].receptor);
    }
    res.view({
      records: records,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  aceptar: async function(req, res) {
    var codNot = req.param('codNot');
    var record = await Notificaciones.findOne({codNot: codNot});

    /*PONEMOS LANOTIFICACION COMO VISTA */
    await Notificaciones.updateOne({
      where : { codNot : codNot}
    }).set({visto : true}).exec((_err, records) =>{});

    /**HACER EL EDIT DE PAREJA Y PONERLE ACORDADO A TRUE */
    if(record.tipo === 0){

      await Pareja.updateOne({
        where : { codCamp : record.aux1, codCategoria : record.aux2, codNivel : record.aux3, codGrupo : record.aux4,  codUsr1 : record.emisor, codUsr2 : req.session.codUsr}
      }).set({estaFormada : true});

      var usr2 = await Usuario.findOne({
        codUsr : record.emisor,
      });
      /*
      var to = usr2.correo;
      var asunto = 'Pareja creada';
      var texto = req.session.codUsr + 'Acepto ser tu pareja en el campeonato';
      let info = await sails.helpers.sendMail(to, asunto, texto);*/

      /**CREAR LA NOTIFICACION PARA EL OTRO */
      var param2 = {
        receptor: record.emisor,
        emisor: req.session.codUsr,
        texto : req.session.nombre + ' acepto ser tu pareja en el campeonato',
        asunto: 'Pareja Campeonato',
        aux1: record.aux1,
        aux2: record.aux2,
        aux3: record.aux3,
        /*tipo: 0 pareja, 1 partido, 2 solo leer*/
        tipo: '2',
        /*aux, en pareja tiene codGrupo */
        aux4: '1', 
        visto : 'false',
      };
      await Notificaciones.create(param2).exec((_err, _res) => {
        res.redirect('/perfil');
      });
    }else if(req.param('tipo') === 1){
      var usr2 = await Usuario.findOne({
        codUsr : record.emisor,
      });
      /*
      var to = usr2.correo;
      var asunto = 'Propuesta enfrentamiento';
      var texto = req.session.codUsr + 'contesto a tu propuesta de fecha de enfrentamiento';
      let info = await sails.helpers.sendMail(to, asunto, texto);*/
      /**HACE LA RESERVA PAL DIA ESPECIFICADO EN LA HORA BUSCANDO UNA PISTA LIBRE */
      /**CREA LA RESERVA PAL DIA EN LA PISTA*/
      var nPistas = await Pista.count();

      await Enfrentamiento.findOne({codEnfrentamiento : record.aux4}).exec((_err, enfrentamiento) =>{
        Reserva.find({ // Reservas coincidentes con el partido
          fecha : new Date(enfrentamiento.fecha).toLocaleDateString(),
          hora : enfrentamiento.hora
        }).exec((_err, reservas) => {
          if(reservas.length === nPistas){  // Si hay tantas pistas como reservas, no hay pistas disponibles
            Enfrentamiento.updateOne({codEnfrentamiento : record.aux4}).set({fecha : '0-0-0'}).exec((_err, _res) =>{});
            var param2 = {
              receptor: record.emisor,
              emisor: req.session.codUsr,
              texto : req.session.nombre + ' acepto tu propuesta pero no quedaban pistas pal dia especificado',
              asunto: 'Enfrentamiento Campeonato',
              aux1: record.aux1,
              aux2: record.aux2,
              aux3: record.aux3,
              /*tipo: 0 pareja, 1 partido, 2 solo lectura */
              tipo: '2',
              /*aux, en pareja tiene codEnfrentamiento */
              aux4: '1', 
              visto : 'false',
            };
            Notificaciones.create(param2).exec((_err, _res) => {
              res.redirect('/perfil');
            });
          }
          else if(reservas.length < nPistas){ // Si hay más pistas que reservas, hay pistas disponibles para el partido
            Pista.find().exec((_err, pistas)=>{
              var codPistas = [];
              for(i = 0; i < pistas.length; i++){
                codPistas.push(pistas[i].codPista);
              }
              var pistasOcupadas = [];
              for(i = 0; i < reservas.length; i++){
                pistasOcupadas.push(reservas[i].codPista);
              }
              var pistasLibres = codPistas.filter(itr => !pistasOcupadas.includes(itr));

              var reserva = {
                fecha: enfrentamiento.fecha,
                hora:  enfrentamiento.hora,
                codUsr: req.session.codUsr,
                codPista: pistasLibres[0]
              };
              Reserva.create(reserva).fetch().exec((_err, nuevaReserva)=>{ // Crea la reserva del partido a nombre del promotor
                Enfrentamiento.updateOne({codEnfrentamiento : record.aux4}).set({codReserva : nuevaReserva.codReserva}).exec((_err, _res)=>{}); // Vincula la nueva reserva al partido
              });
            });
            var param2 = {
              receptor: record.aux4,
              emisor: req.session.codUsr,
              texto : req.session.nombre + ' acepto tu propuesta y se creo la reserva',
              aux1: record.aux1,
              aux2: record.aux2,
              aux3: record.aux3,
              asunto: 'Efrentamiento Campeonato',
              /*tipo: 0 pareja, 1 partido, 2 solo lectura */
              tipo: '2',
              /*aux, en pareja tiene codEnfrentamiento */
              aux4: '1', 
              visto : 'false',
            };
            Notificaciones.create(param2).exec((_err, _res) => {
              res.redirect('/perfil');
            });
          }
        });
      });
      /**NOTIFICA AL OTRO QUE SE CREO LA RESERVA Y LA PISTA*/
      var param2 = {
        receptor: record.emisor,
        emisor: req.session.codUsr,
        texto : req.session.nombre + ' acepto tu propuesta de fecha',
        asunto: 'Enfrentamiento Campeonato',
        aux1: record.aux1,
        aux2: record.aux2,
        aux3: record.aux3,
        /*tipo: 0 pareja, 1 partido, 2 solo lectura */
        tipo: '2',
        /*aux, en pareja tiene codEnfrentamiento */
        aux4: '1', 
        visto : 'false',
      };
      await Notificaciones.create(param2).exec((_err, _res) => {
        res.redirect('/perfil');
      });
    } 
  },
  rechazar: async function(req, res) {
    var codNot = req.param('codNot');
    var record = await Notificaciones.findOne({codNot: codNot});
    /*PONEMOS LANOTIFICACION COMO VISTA */
    await Notificaciones.updateOne({
      where : { codNot : codNot}
    }).set({visto : true}).exec((_err, records) =>{});
    
    if(record.tipo === 0){

      var usr2 = await Usuario.findOne({
        codUsr : record.emisor,
      });
      /*
      var to = usr2.correo;
      var asunto = 'Pareja rechazada';
      var texto =  req.session.codUsr + 'No acepto ser tu pareja en el campeonato';
      let info = await sails.helpers.sendMail(to, asunto, texto);*/

      /**ELIMINAR LA PAREJA DE LA BD*/
      await Pareja.destroyOne({
        where : { codCamp : record.aux1, codCategoria : record.aux2, codNivel : record.aux3, codGrupo : record.aux4,  codUsr1 : record.emisor, codUsr2 : req.session.codUsr}
      }).exec((_err, _res) => {});
      /**CREAR LA NOTIFICACION PARA EL OTRO */
      var param2 = {
        receptor: record.emisor,
        emisor: req.session.codUsr,
        texto : req.session.nombre + ' no acepto ser tu pareja en el campeonato',
        asunto: 'Pareja Campeonato',
        aux1: record.aux1,
        aux2: record.aux2,
        aux3: record.aux3,
        /*tipo: 0 pareja, 1 partido, 2 solo lectura*/
        tipo: '2',
        /*aux, en pareja tiene codGrupo */
        aux4: '1', 
        visto : 'false',
      };
      await Notificaciones.create(param2).exec((_err, _res) => {
        res.redirect('/perfil');
      });
    }
    if(record.tipo === 1){
      var usr2 = await Usuario.findOne({
        codUsr : record.emisor,
      });
      /*
      var to = usr2.correo;
      var asunto = 'Pareja rechazada';
      var texto =  req.session.codUsr + 'No acepto el horario que le propusiste';
      let info = await sails.helpers.sendMail(to, asunto, texto);*/
      
      /**PONE EN ENFRENTAMIENTO LA FECHA COMO 0-0-0 OTRA VEZ*/
      await Enfrentamiento.updateOne({codEnfrentamiento : record.aux4})
      .set({fecha : '0-0-0'}).exec((_err, _res) =>{});
      /**CREAR LA NOTIFICACION PARA EL OTRO */
      var param2 = {
        receptor: record.emisor,
        emisor: req.session.codUsr,
        texto : req.session.nombre + ' no acepto el horario que le propusiste',
        aux1: record.aux1,
        aux2: record.aux2,
        aux3: record.aux3,
        asunto: 'Enfrentamiento Campeonato',
        /*tipo: 0 pareja, 1 partido, 2 solo lectura */
        tipo: '2',
        /*aux, en pareja tiene codEnfrentamiento */
        aux4: record.aux4, 
        visto : 'false',
      };
      await Notificaciones.create(param2).exec((_err, _res) => {
        res.redirect('/perfil');
      });
    }
  },
  aceptarClase: async function(req, res) {
    var datos = await Notificaciones.findOne({codNot: req.param('codNot')});
    var codNot = datos.codNot;
    var emisor = datos.emisor;
    var aux1 = datos.aux1;
    var clase = await ClaseParticular.findOne({codClase : aux1});   
    await Notificaciones.updateOne({codNot: codNot}).set({visto: '1'});
    var fecha = new Date(new Date(clase.fecha).getTime() - (new Date(clase.fecha).getTimezoneOffset() * 60000 )).toISOString().split("T")[0];
    var numPistas = await Pista.count();
    var numReservas = await Reserva.count({fecha : fecha, hora : clase.hora});
    if(numReservas === numPistas){ 
      await ClaseParticular.destroyOne({codClase : clase.codClase});
      var param2 = {
        receptor: emisor,
        emisor: req.session.codUsr,
        asunto: 'Petición de clase cancelada',
        texto : 'Tu petición de clase ha sido aceptada por el profesor, pero no había pistas disponibles en la fecha y hora acordada.',
        tipo: '2',
        visto : 'false',
      };
      await Notificaciones.create(param2).exec((_err, _res) => {
        res.redirect('/perfil');
      });
    } 
    
    else{
      var i = 1;
      while(i <= numPistas){
        numOcupadas = await Reserva.count({fecha : fecha, hora : clase.hora, codPista : i});
        if(numOcupadas > 0){
          i++;
        }
        else{
          var pista = i;
          i = numPistas+1;
        }
      }
      var reserva = {
        fecha: clase.fecha,
        hora:  clase.hora,
        codUsr: req.session.codUsr,
        codPista: pista
      };
      Reserva.create(reserva).fetch().exec((_err, nuevaReserva)=>{
        ClaseParticular.updateOne({
          where : { codClase : aux1}
        }).set({codReserva : nuevaReserva.codReserva, pista : pista}).exec((_err, records) =>{});
      });
      var precio = await Precios.findOne({codPrecio: '5'}); // Consulta el precio del servicio
      var precioFinal = precio.precio; 
      var param2 = {
        receptor: emisor,
        emisor: req.session.codUsr,
        asunto: 'Petición de clase aceptada',
        texto : 'Tu petición de clase ha sido aceptada por el profesor. Tendrá un coste de ' + precioFinal + '€.',
        tipo: '2',
        visto : 'false',
      };
      await Notificaciones.create(param2).exec((_err, _res) => {
        res.redirect('/perfil');
      });
    }
  },
  rechazarClase: async function(req, res) {
    var datos = await Notificaciones.findOne({codNot: req.param('codNot')});
    var codNot = datos.codNot;
    var emisor = datos.emisor;
    var aux1 = datos.aux1;

    await Notificaciones.updateOne({codNot: codNot}).set({visto: '1'});
    /*SUPRIMIR LA CLASE PARTICULAR*/
    await ClaseParticular.destroyOne({codClase : aux1});
    /**CREA LA NOTIFICACIÓN INFORMANDO DEL RECHAZO */
    var param2 = {
      receptor: emisor,
      emisor: req.session.codUsr,
      asunto: 'Petición de clase cancelada',
      texto : 'Tu petición de clase ha sido rechazada por el profesor.',
      tipo: '2',
      visto : 'false',
    };
    await Notificaciones.create(param2).exec((_err, _res) => {
      res.redirect('/perfil');
    });
  }
};
