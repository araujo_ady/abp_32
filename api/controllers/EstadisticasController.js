/**
 * EstadisticasController
 *
 * @description :: Muestra los enfrentamientos y permite escoger fecha
 */

module.exports = {
  show: async function(req, res) {

    //Miramos sus enfrentamientos en ParejasEnfrentamiento
    var partidosTotales = await ParejasEnfrentamiento.find({
      or : [
        {capitan : req.session.codUsr},
        {jugador : req.session.codUsr}
      ]
    });
        
    //De todos sus enfrentamientos miramos los que ya pasaron, fecha<hoy.
    //Recorremos todos sus enfrentamientos, vamos mirando si la pasaron, si es asi guardamos su codEnfrentamiento
    var codPartidosPasados = [];
        
    for(var i=0; i<partidosTotales.length;i++){
      var partidoActual = await Enfrentamiento.findOne({where: {codEnfrentamiento : partidosTotales[i].codEnfrentamiento}});

      if(partidoActual.fecha < new Date()){
        codPartidosPasados.push(partidoActual.codEnfrentamiento);
      }
    }

    //Una vez tenemo todos los partidos pasados vamos uno por uno sacando sus dos tuplas
    //la suya y la de la pareja contra la que iban, y vamos incrementando las distintas variables
    var partidosGanados = 0;
    var partidosPerdidos = 0;
    var partidosGanadosSinPerderSets = 0; //Miramos cuantos sets gano el otro
    var partidosGanadosPerdiendoUnSet = 0;//se saca de la diferencia de partidosGanados-partidosGanadosSinPerderSets
    var partidosPerdidosSinGanarSets = 0; //Miramos cuantos sets gano
    var partidosPerdidosGanandoUnSet = 0; //se saca de la diferencia de partidosPerdidos-partidosPerdidosSinGanarSets
    
    for(var j=0; j<codPartidosPasados.length;j++){
      //Cogemos ambas tuplas del partido
      var partido = await ParejasEnfrentamiento.find({codEnfrentamiento : codPartidosPasados[j]});

      //Miramos si es mi tupla
      if((partido[0].capitan == req.session.codUsr) || (partido[0].jugador == req.session.codUsr)){
        //Miramos si gano o perdio
        if(partido[0].resultado > partido[1].resultado){
          partidosGanados++;
          //Si gano miramos si lo hizo sin perder sets
          if(partido[1].resultado == 0){
            partidosGanadosSinPerderSets++;
          }else{
            partidosGanadosPerdiendoUnSet++;
          }
        }else{ //Si entrapor aqui perdio el partido
          partidosPerdidos++;
          if(partido[0].resultado == 0){
            partidosPerdidosSinGanarSets++;
          }else{
            partidosPerdidosGanandoUnSet++;
          }
        }
      }else{ // Si no entro por el if mi tupla es esta.
        //Miramos si gano o perdio
        if(partido[1].resultado > partido[0].resultado){
          partidosGanados++;
          //Si gano miramos si lo hizo sin perder sets
          if(partido[0].resultado == 0){
            partidosGanadosSinPerderSets++;
          }else{
            partidosGanadosPerdiendoUnSet++;
          }
        }else{ //Si entrapor aqui perdio el partido
          partidosPerdidos++;
          if(partido[1].resultado == 0){
            partidosPerdidosSinGanarSets++;
          }else{
            partidosPerdidosGanandoUnSet++;
          }
        }
      }
    }
    //Al acabar el for tenemos los datos de todos los partidos, solo falta pasarselo a la vista
    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      partidosGanados: partidosGanados,
      partidosPerdidos: partidosPerdidos,
      partidosGanadosSinPerderSets: partidosGanadosSinPerderSets,
      partidosGanadosPerdiendoUnSet: partidosGanadosPerdiendoUnSet,
      partidosPerdidosSinGanarSets: partidosPerdidosSinGanarSets,
      partidosPerdidosGanandoUnSet: partidosPerdidosGanandoUnSet,
      session : req.session
    });
  },
  clasificacion: async function(req, res) {
    //Variable a retornar
    var toret = [];

    //Sacamos la lista de todos los usuarios
    var usuarios = await Usuario.find();


    //Vamos usuario por usuario sacando su nombre, apellidos, partidos ganados y partidos totales
    for(var x=0; x < usuarios.length; x++){
      //Miramos sus enfrentamientos en ParejasEnfrentamiento
      var partidosTotales = await ParejasEnfrentamiento.find({
        or : [
          {capitan : usuarios[x].codUsr},
          {jugador : usuarios[x].codUsr}
        ]
      });
                
      //De todos sus enfrentamientos miramos los que ya pasaron, fecha<hoy.
      //Recorremos todos sus enfrentamientos, vamos mirando si la pasaron, si es asi guardamos su codEnfrentamiento
      var codPartidosPasados = [];
                
      for(var i=0; i<partidosTotales.length;i++){
        var partidoActual = await Enfrentamiento.findOne({where: {codEnfrentamiento : partidosTotales[i].codEnfrentamiento}});
            
        if(partidoActual.fecha < new Date()){
          codPartidosPasados.push(partidoActual.codEnfrentamiento);
        }
      }
            
      //Una vez tenemo todos los partidos pasados vamos uno por uno sacando sus dos tuplas
      //la suya y la de la pareja contra la que iban, y vamos incrementando las distintas variables
      var partidosGanados = 0;
      var partidosTotales = 0;
            
      for(var j=0; j<codPartidosPasados.length;j++){
        //Cogemos ambas tuplas del partido
        var partido = await ParejasEnfrentamiento.find({codEnfrentamiento : codPartidosPasados[j]});
            
        //Miramos si es mi tupla
        if((partido[0].capitan == usuarios[x].codUsr) || (partido[0].jugador == usuarios[x].codUsr)){
          //Miramos si gano o perdio
          if(partido[0].resultado > partido[1].resultado){
            partidosGanados++;
          }
        }else{ // Si no entro por el if mi tupla es esta.
          //Miramos si gano o perdio
          if(partido[1].resultado > partido[0].resultado){
            partidosGanados++;
          }
        }
        //Gane o pierda incrementamos la variable de partidos totales
        partidosTotales++;
      }
      //Guardamos nombre apellidos partidos ganados y partidos totales en la variable a devolver
      var datos = {
        nombre: usuarios[x].nombre,
        apellidos: usuarios[x].apellidos,
        partidosGanados: partidosGanados,
        partidosTotales: partidosTotales,
      };

      toret.push(datos);
    }

    //Al acabar el for tenemos los datos de todos los usuarios, hay que ordenarlos.
    for(var recorrido=0; recorrido<toret.length; recorrido++){

      var maxGanados = 0;
      var mayor;

      for(var z=recorrido; z<toret.length; z++){

        if(toret[z].partidosGanados > maxGanados){
          maxGanados = toret[z].partidosGanados;
          mayor = toret[z];
          toret[z] = toret[recorrido];
          toret[recorrido] = mayor;
        }
      }
    }

    //Al salir del for toret esta ordenado, solo falta pasarselo a la vista.
    res.view({
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'}),
      toret: toret,
      session : req.session
    });
  },
};
  