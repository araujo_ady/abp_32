/**
 * PreciosController
 *
 * @description :: Muestra la tabla de precios, y permite su modificación para usuarios normales y socios.
 */

module.exports = {
  show: async function(req, res) {
    records = await Precios.find();
    res.view({
      records: records,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  edit: async function(req, res) {
    var record = await Precios.findOne({
      codPrecio : req.param('codPrecio')
    });
    res.view({
      record: record,
      session : req.session,
      notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
    });
  },
  confirmEdit: async function(req, res) {
    var servicio = {
      codPrecio : req.param('codPrecio')
    };
    var precios = {
      precio: req.param('precio'),
      precioSocio: req.param('precioSocio')
    };
    await Precios.updateOne(servicio).set(precios);
    res.redirect('/gestionarprecios');
  }
};

