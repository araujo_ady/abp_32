/**
 * ClaseParticularController
 *
 * @description :: Permite realizar y aceptar una solicitud de clase particular.
 */

module.exports = {

    show: async function(req, res) {
      var records = await Usuario.find({tipo: 1});
      res.view({
        records: records,
        session : req.session,
        notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
      });
    },

    showInscrito: async function(req, res) {
      var records = await ClaseParticular.find({alumno: req.session.codUsr});
      var nombres = [];
      for(i=0; i<records.length;i++){
        nombres.push(await sails.helpers.getNombreEntrenador(records[i].entrenador));
      }
      res.view({
        tipo : req.session.tipo,
        records: records,
        nombres: nombres,
        session : req.session,
        notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
      });
    },

    proponerFecha: async function(req, res) {
        var entrenador = req.param('entrenador'); //Recogemos el entrenador a solicitar
        res.view({
          entrenador: entrenador,
          notificaciones: await Notificaciones.count({receptor: req.session.codUsr, visto: '0'})
        });      
      },

      insert: async function(req, res) {
        var fecha = req.param('fecha');
        var dia = new Date();
        dia.setDate(dia.getDate() + (fecha/10)); // Esto es la fecha
        var hora = parseInt(fecha%10); //La hora de 0 a 7
        var horaBien; //La hora como string
        switch(hora){
          case 0:
            horaBien = "9:00";
            break;
          case 1:
            horaBien = "10:30";
            break;
          case 2:
            horaBien = "12:00";
            break;
          case 3:
            horaBien = "13:30";
            break;
          case 4:
            horaBien = "15:00";
            break;
          case 5:
            horaBien = "16:30";
            break;
          case 6:
            horaBien = "18:00";
            break;
          case 7:
            horaBien = "19:30";
            break;
        }
        var param = {
          entrenador: req.param('entrenador'),
          fecha: dia,
          hora: hora,
          alumno: req.session.codUsr,
        };
        await ClaseParticular.create(param).fetch().exec((_err, records) => {
          var param2 = {
            receptor: req.param('entrenador'),
            emisor: req.session.codUsr,
            tipo: '3',
            aux1: records.codClase,
            asunto: 'Propuesta de Clase Particular',
            texto : '¿Quieres impartir una clase particular el dia ' + dia.toLocaleDateString() + ', a la hora ' + horaBien + '?',
            visto : 'false',
          };
          Notificaciones.create(param2).exec((_err, _res) => {
            res.redirect('/gestionarclaseparticular');
           });
        });
      },
};