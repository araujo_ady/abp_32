/*
SQLyog Community
MySQL - 5.5.39 : Database - abp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP DATABASE IF EXISTS `abp`;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`abp` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;

USE `abp`;

/*Table structure for table `usuario` */

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `codUsr` INT(11) NOT NULL AUTO_INCREMENT,
  `correo` VARCHAR(320) COLLATE utf8_spanish_ci DEFAULT NULL,
  `password` VARCHAR(90) COLLATE utf8_spanish_ci DEFAULT NULL,
  `nombre` VARCHAR(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellidos` VARCHAR(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechaNac` DATE DEFAULT NULL,
  `genero` VARCHAR(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `esSocio` TINYINT(1) DEFAULT NULL,
  `tipo` TINYINT(3) DEFAULT NULL,
  `avatar` VARCHAR(255) COLLATE utf8_spanish_ci DEFAULT 'estandar',
  `banco` VARCHAR(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hash` VARCHAR(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`codUsr`)
) ENGINE=INNODB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `usuario` */

INSERT  INTO `usuario`(`codUsr`,`correo`,`password`,`nombre`,`apellidos`,`fechaNac`,`genero`,`esSocio`,`tipo`,`avatar`,`banco`,`hash`) VALUES 
(1,'aagregorio@esei.uvigo.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$qZnkrh0n1xHlrUPu$AbHIqw3JsFU=$uXApIfwf0Nv7R9OANzjb0w','Adrian','Araujo','1996-10-29','hombre',0,0,'estandar','',NULL),
(2,'avgarcia@esei.uvigo.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Alex','Villanueva','1996-07-05','hombre',1,0,'estandar','',NULL),
(3,'cfgarrido@esei.uvigo.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Cristhian','Ferreiro','1997-04-04','hombre',0,0,'estandar','',NULL),
(4,'jgmarchena@esei.uvigo.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Julio','Gonzalez','1991-03-04','hombre',1,0,'estandar','',NULL),
(5,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Celso','Campos','1970-01-01','hombre',0,0,'estandar','',NULL),
(6,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Pedro','Cuesta','1970-01-01','hombre',0,0,'estandar','',NULL),
(7,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Arno','Formella','1970-01-01','hombre',0,0,'estandar','',NULL),
(8,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Miguel Reboeiro','','1970-01-01','hombre',0,0,'estandar','',NULL),
(9,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Francisco Jose','Ribadas','1970-01-01','hombre',0,0,'estandar','',NULL),
(10,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Manuel','Vilares','1970-01-01','hombre',0,0,'estandar','',NULL),
(11,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Sergio','Ramos','1970-01-01','hombre',0,0,'estandar','',NULL),
(12,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Santiago','Cazorla','1970-01-01','hombre',0,0,'estandar','',NULL),
(13,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','David','Silva','1970-01-01','hombre',0,0,'estandar','',NULL),
(14,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Amancio','Amaro','1970-01-01','hombre',0,0,'estandar','',NULL),
(15,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Fabian','Ruiz','1970-01-01','hombre',0,0,'estandar','',NULL),
(16,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Rafael','Nadal','1970-01-01','hombre',0,0,'estandar','',NULL),
(17,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Daniil','Mevdevev','1970-01-01','hombre',0,0,'estandar','',NULL),
(18,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Roberto','Bautista','1970-01-01','hombre',0,0,'estandar','',NULL),
(19,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Pablo','Carreno','1970-01-01','hombre',0,0,'estandar','',NULL),
(20,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Fernando','Verdasco','1970-01-01','hombre',0,0,'estandar','',NULL),
(21,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Marc','Gasol','1970-01-01','hombre',0,0,'estandar','',NULL),
(22,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Ricky','Rubio','1970-01-01','hombre',0,0,'estandar','',NULL),
(23,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Luka','Doncic','1970-01-01','hombre',0,0,'estandar','',NULL),
(24,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Sergio','Llull','1970-01-01','hombre',0,0,'estandar','',NULL),
(25,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','LeBron','James','1970-01-01','hombre',0,0,'estandar','',NULL),
(26,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Francisco','Navarro','1970-01-01','hombre',0,0,'estandar','',NULL),
(27,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Alejandro','Galan','1970-01-01','hombre',0,0,'estandar','',NULL),
(28,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Carlos Daniel','Gutierrez','1970-01-01','hombre',0,0,'estandar','',NULL),
(29,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Maximiliano','Sanchez','1970-01-01','hombre',0,0,'estandar','',NULL),
(30,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Fernando','Belasteguin','1970-01-01','hombre',0,0,'estandar','',NULL),
(31,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Gonzalo','Jacome','1970-01-01','hombre',0,0,'estandar','',NULL),
(32,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Jesus','Vazquez','1970-01-01','hombre',0,0,'estandar','',NULL),
(33,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Manuel','Cabezas','1970-01-01','hombre',0,0,'estandar','',NULL),
(34,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Francisco','Rodriguez','1970-01-01','hombre',0,0,'estandar','',NULL),
(35,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Jose Luis','Baltar','1970-01-01','hombre',0,0,'estandar','',NULL),
(36,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Alejandro','Magno','1970-01-01','hombre',0,0,'estandar','',NULL),
(37,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Julio','Cesar','1970-01-01','hombre',0,0,'estandar','',NULL),
(38,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Napoleon','Bonaparte','1970-01-01','hombre',0,0,'estandar','',NULL),
(39,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Hernan','Cortes','1970-01-01','hombre',0,0,'estandar','',NULL),
(40,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Gengis','Khan','1970-01-01','hombre',0,0,'estandar','',NULL),
(41,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Justin','Bieber','1970-01-01','hombre',0,0,'estandar','',NULL),
(42,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Justin','Timberlake','1970-01-01','hombre',0,0,'estandar','',NULL),
(43,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Drake','Graham','1970-01-01','hombre',0,0,'estandar','',NULL),
(44,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Juan Luis','Maluma','1970-01-01','hombre',0,0,'estandar','',NULL),
(45,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Zayn','Malik','1970-01-01','hombre',0,0,'estandar','',NULL),
(46,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Rami','Malek','1970-01-01','hombre',0,0,'estandar','',NULL),
(47,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Christian','Bale','1970-01-01','hombre',0,0,'estandar','',NULL),
(48,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Bryan','Cranston','1970-01-01','hombre',0,0,'estandar','',NULL),
(49,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Michael','Fassbender','1970-01-01','hombre',0,0,'estandar','',NULL),
(50,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Matthew','McConaughey','1970-01-01','hombre',0,0,'estandar','',NULL),
(51,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Monkey','Luffy','1970-01-01','hombre',0,0,'estandar','',NULL),
(52,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','edward','Elrick','1970-01-01','hombre',0,0,'estandar','',NULL),
(53,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Light','Yagami','1970-01-01','hombre',0,0,'estandar','',NULL),
(54,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Shinnosuke','Nohara','1970-01-01','hombre',0,0,'estandar','',NULL),
(55,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Eren','Jaeger','1970-01-01','hombre',0,0,'estandar','',NULL),
(56,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Anthony','Stark','1970-01-01','hombre',0,0,'estandar','',NULL),
(57,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Miles','Morales','1970-01-01','hombre',0,0,'estandar','',NULL),
(58,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Bruce','Banner','1970-01-01','hombre',0,0,'estandar','',NULL),
(59,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Stephen','Strange','1970-01-01','hombre',0,0,'estandar','',NULL),
(60,'email@email.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Bruce','Wayne','1970-01-01','hombre',0,0,'estandar','',NULL),
(62,'admin@admin.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','Admin','Admin','1970-10-01','hombre',1,2,'estandar','',NULL),
(63,'entrenador@entrenador.es','YWVzLTI1Ni1nY20kJGRlZmF1bHQ=$iJWto/r2j6b4Mz7X$JRsNNSZX4Kw=$j7NwDib+8qy3OpSMwLK//g','entren','entren','1970-10-01','hombre',1,1,'estandar','',NULL);

/*Table structure for table `pista` */

DROP TABLE IF EXISTS `pista`;

CREATE TABLE `pista` (
  `codPista` INT(11) NOT NULL AUTO_INCREMENT,
  `superficie` VARCHAR(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `interior` TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (`codPista`),
  UNIQUE KEY `codPista` (`codPista`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `pista` */

INSERT  INTO `pista`(`codPista`, `superficie`, `interior`) VALUES 
(1, 'Cemento', 0),
(2, 'Cesped', 1);

/*Table structure for table `reserva` */

DROP TABLE IF EXISTS `reserva`;

CREATE TABLE `reserva` (
  `codReserva` INT(11) NOT NULL AUTO_INCREMENT,
  `fecha` DATE DEFAULT NULL,
  `hora` TINYINT(3) DEFAULT NULL,
  `codUsr` INT(11) DEFAULT NULL,
  `codPista` INT(11) DEFAULT NULL,
  PRIMARY KEY (`codReserva`),
  UNIQUE KEY `codReserva` (`codReserva`),
  KEY `FK - reserva - pista` (`codPista`),
  CONSTRAINT `FK - reserva - pista` FOREIGN KEY (`codPista`) REFERENCES `pista` (`codPista`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `reserva` */

INSERT  INTO `reserva`(`codReserva`,`fecha`,`hora`,`codUsr`,`codPista`) VALUES 
(1,'2019-11-23',1,6,2),
(2,'2019-11-23',2,7,2),
(3,'2019-11-23',3,8,2),
(4,'2019-11-23',4,9,2),
(5,'2019-11-23',5,10,2),
(6,'2019-11-23',6,11,2),
(7,'2019-11-23',7,12,2),
(8,'2019-11-24',0,13,2),
(9,'2019-11-24',1,14,2),
(10,'2019-11-24',2,15,2),
(11,'2019-11-24',3,16,2),
(12,'2019-11-24',4,17,2),
(13,'2019-11-24',5,18,2),
(14,'2019-11-24',6,19,2),
(15,'2019-11-24',7,20,2),
(16,'2019-11-23',0,21,1),
(17,'2019-11-23',1,22,1),
(18,'2019-11-23',3,23,1),
(19,'2019-11-23',4,24,1),
(20,'2019-11-23',7,25,1),
(21,'2019-11-24',0,26,1),
(22,'2019-11-24',1,27,1),
(23,'2019-11-24',2,28,1),
(24,'2019-11-24',3,29,1),
(25,'2019-11-24',5,30,1),
(26,'2019-11-24',6,31,1),
(27,'2019-11-23',34,5,1);

/*Table structure for table `partido` */

DROP TABLE IF EXISTS `partido`;

CREATE TABLE `partido` (
  `codPartido` INT(11) NOT NULL AUTO_INCREMENT,
  `fecha` DATE NOT NULL,
  `hora` TINYINT(3) NOT NULL,
  `cancelado` TINYINT(1) NOT NULL,
  `promotor` INT(11) NOT NULL,
  `codReserva` INT(11) DEFAULT NULL,
  PRIMARY KEY (`codPartido`),
  UNIQUE KEY `codPartido` (`codPartido`),
  KEY `FK - partido - usuario` (`promotor`),
  KEY `FK - partido - reserva` (`codReserva`),
  CONSTRAINT `FK - partido - reserva` FOREIGN KEY (`codReserva`) REFERENCES `reserva` (`codReserva`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK - partido - usuario` FOREIGN KEY (`promotor`) REFERENCES `usuario` (`codUsr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `partido` */

INSERT  INTO `partido`(`codPartido`,`fecha`,`hora`,`cancelado`,`promotor`,`codReserva`) VALUES 
(1,'2019-11-23',5,0,62,NULL),
(2,'2019-11-23',6,0,62,NULL),
(3,'2019-11-24',4,0,62,NULL),
(4,'2019-11-24',7,0,62,NULL),
(5,'2019-11-23',2,0,62,NULL);

/*Table structure for table `jugadorespartido` */

DROP TABLE IF EXISTS `jugadorespartido`;

CREATE TABLE `jugadorespartido` (
  `jugador` INT(11) NOT NULL,
  `partido` INT(11) NOT NULL,
  `resultado` INT(11) DEFAULT NULL,
  PRIMARY KEY (`partido`,`jugador`),
  UNIQUE KEY `partido` (`partido`,`jugador`),
  KEY `FK - jugadorespartido - usuario` (`jugador`),
  CONSTRAINT `FK - jugadorespartido - usuario` FOREIGN KEY (`jugador`) REFERENCES `usuario` (`codUsr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK - jugadorespartido - partido` FOREIGN KEY (`partido`) REFERENCES `partido` (`codPartido`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `jugadorespartido` */

/*Table structure for table `alumnosescuela` */

DROP TABLE IF EXISTS `alumnosescuela`;

CREATE TABLE `alumnosescuela` (
  `alumno` INT(11) NOT NULL DEFAULT '0',
  `escuela` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`alumno`,`escuela`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `alumnosescuela` */

INSERT  INTO `alumnosescuela`(`alumno`,`escuela`) VALUES 
(1,2),
(2,2),
(3,2),
(4,2),
(5,1),
(5,2),
(6,1),
(6,2),
(7,1),
(7,2),
(8,1),
(9,1);

/*Table structure for table `archive` */

DROP TABLE IF EXISTS `archive`;

CREATE TABLE `archive` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `createdAt` BIGINT(20) DEFAULT NULL,
  `fromModel` VARCHAR(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `originalRecord` LONGTEXT COLLATE utf8_spanish_ci,
  `originalRecordId` LONGTEXT COLLATE utf8_spanish_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `archive` */

/*Table structure for table `campeonato` */

DROP TABLE IF EXISTS `campeonato`;

CREATE TABLE `campeonato` (
  `codCamp` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechaInicio` DATE DEFAULT NULL,
  `fechaFin` DATE DEFAULT NULL,
  PRIMARY KEY (`codCamp`),
  UNIQUE KEY `codCamp` (`codCamp`)
) ENGINE=INNODB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `campeonato` */

INSERT  INTO `campeonato`(`codCamp`,`nombre`,`fechaInicio`,`fechaFin`) VALUES 
(2,'Campeonato de Navidad','2019-12-20','2020-01-24');

/*Table structure for table `categoria` */

DROP TABLE IF EXISTS `categoria`;

CREATE TABLE `categoria` (
  `codCamp` INT(11) DEFAULT NULL,
  `codCategoria` INT(11) NOT NULL DEFAULT '0',
  `nombre` VARCHAR(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`codCamp`,`codCategoria`),
  UNIQUE KEY `codCategoria` (`codCamp`,`codCategoria`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `categoria` */

INSERT  INTO `categoria`(`codCamp`,`codCategoria`,`nombre`) VALUES 
(2,1,'masculino'),
(2,2,'femenina'),
(2,3,'mixta');

/*Table structure for table `enfrentamiento` */

DROP TABLE IF EXISTS `enfrentamiento`;

CREATE TABLE `enfrentamiento` (
  `codEnfrentamiento` INT(11) NOT NULL AUTO_INCREMENT,
  `codCamp` INT(11) DEFAULT NULL,
  `codCategoria` INT(11) DEFAULT NULL,
  `codNivel` INT(11) DEFAULT NULL,
  `fecha` DATE DEFAULT NULL,
  `hora` TINYINT(3) DEFAULT NULL,
  `fase` TINYINT(3) DEFAULT NULL,
  `codReserva` INT(11) DEFAULT NULL,
  PRIMARY KEY (`codEnfrentamiento`),
  UNIQUE KEY `codEnfrentamiento` (`codEnfrentamiento`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `enfrentamiento` */

/*Table structure for table `escueladeportiva` */

DROP TABLE IF EXISTS `escueladeportiva`;

CREATE TABLE `escueladeportiva` (
  `codEsc` INT(11) NOT NULL AUTO_INCREMENT,
  `entrenador` INT(11) DEFAULT NULL,
  `nombre` VARCHAR(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechaInicio` DATE DEFAULT NULL,
  `fechaFin` DATE DEFAULT NULL,
  `fecha` DATE DEFAULT NULL,
  `hora` TINYINT(3) DEFAULT NULL,
  `cupoMin` INT(11) DEFAULT NULL,
  `cupoMax` INT(11) DEFAULT NULL,
  `numClases` INT(11) DEFAULT NULL,
  PRIMARY KEY (`codEsc`),
  UNIQUE KEY `codEsc` (`codEsc`)
) ENGINE=INNODB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `escueladeportiva` */

INSERT  INTO `escueladeportiva`(`codEsc`,`entrenador`,`nombre`,`fechaInicio`,`fechaFin`,`fecha`,`hora`,`cupoMin`,`cupoMax`,`numClases`) VALUES 
(1,63,'Escuela de Pelar Patatas','2019-12-20','2019-12-22','2019-12-24',1,5,10,1),
(2,63,'Escuela Periodica','2020-01-01','2020-01-05','2020-01-10',1,5,15,5);

/*Table structure for table `claseparticular` */

DROP TABLE IF EXISTS `claseparticular`;

CREATE TABLE `claseparticular` (
  `codClase` INT(11) NOT NULL AUTO_INCREMENT,
  `entrenador` INT(11) DEFAULT NULL,
  `alumno` INT(11) DEFAULT NULL,
  `fecha` DATE DEFAULT NULL,
  `hora` TINYINT(3) DEFAULT NULL,
  `pista` INT(11) DEFAULT NULL,
  `codReserva` INT(11) DEFAULT NULL,
  PRIMARY KEY (`codClase`),
  UNIQUE KEY `codClase` (`codClase`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Table structure for table `grupo` */

DROP TABLE IF EXISTS `grupo`;

CREATE TABLE `grupo` (
  `codCamp` INT(11) DEFAULT NULL,
  `codCategoria` INT(11) DEFAULT NULL,
  `codNivel` INT(11) DEFAULT NULL,
  `codGrupo` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codCamp`,`codCategoria`,`codNivel`,`codGrupo`),
  UNIQUE KEY `codGrupo` (`codCamp`,`codCategoria`,`codNivel`,`codGrupo`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `grupo` */

/*Table structure for table `nivel` */

DROP TABLE IF EXISTS `nivel`;

CREATE TABLE `nivel` (
  `codCamp` INT(11) DEFAULT NULL,
  `codCategoria` INT(11) DEFAULT NULL,
  `codNivel` INT(11) NOT NULL DEFAULT '0',
  `nombre` VARCHAR(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`codCamp`,`codCategoria`,`codNivel`),
  UNIQUE KEY `codNivel` (`codCamp`,`codCategoria`,`codNivel`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `nivel` */

/*Table structure for table `notificaciones` */

DROP TABLE IF EXISTS `notificaciones`;

CREATE TABLE `notificaciones` (
  `codNot` INT(11) NOT NULL AUTO_INCREMENT,
  `receptor` INT(11) DEFAULT NULL,
  `emisor` INT(11) DEFAULT NULL,
  `tipo` TINYINT(3) DEFAULT NULL,
  `aux1` INT(11) DEFAULT NULL,
  `aux2` INT(11) DEFAULT NULL,
  `aux3` INT(11) DEFAULT NULL,
  `aux4` INT(11) DEFAULT NULL,
  `asunto` VARCHAR(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `texto` TEXT COLLATE utf8_spanish_ci,
  `visto` TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (`codNot`),
  UNIQUE KEY `codNot` (`codNot`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `notificaciones` */

/*Table structure for table `pareja` */

DROP TABLE IF EXISTS `pareja`;

CREATE TABLE `pareja` (
  `codCamp` INT(11) NOT NULL DEFAULT '0',
  `codCategoria` INT(11) DEFAULT NULL,
  `codNivel` INT(11) DEFAULT NULL,
  `codGrupo` INT(11) DEFAULT NULL,
  `codUsr1` INT(11) DEFAULT NULL,
  `codUsr2` INT(11) DEFAULT NULL,
  `estaFormada` TINYINT(1) DEFAULT NULL,
  `puntuacionLigaRegular` DOUBLE DEFAULT NULL,
  PRIMARY KEY (`codCamp`, `codCategoria`, `codNivel`, `codGrupo`),
  UNIQUE KEY `parejas` (`codCamp`, `codCategoria`, `codNivel`, `codGrupo`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `pareja` */

/*Table structure for table `parejasenfrentamiento` */

DROP TABLE IF EXISTS `parejasenfrentamiento`;

CREATE TABLE `parejasenfrentamiento` (
  `codEnfrentamiento` INT(11) NOT NULL DEFAULT '0',
  `codCamp` INT(11) DEFAULT NULL,
  `codCategoria` INT(11) DEFAULT NULL,
  `codNivel` INT(11) DEFAULT NULL,
  `codGrupo` INT(11) DEFAULT NULL,
  `capitan` INT(11) DEFAULT NULL,
  `jugador` INT(11) DEFAULT NULL,
  `resultado` INT(11) DEFAULT NULL,
  PRIMARY KEY (`codEnfrentamiento`),
  UNIQUE KEY `codEnfrentamiento` (`codEnfrentamiento`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `parejasenfrentamiento` */

/*Table structure for table `precios` */

DROP TABLE IF EXISTS `precios`;

CREATE TABLE `precios` (
  `codPrecio` INT(11) NOT NULL AUTO_INCREMENT,
  `servicio` VARCHAR(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `precio` FLOAT DEFAULT NULL,
  `precioSocio` FLOAT DEFAULT NULL,
  PRIMARY KEY (`codPrecio`),
  UNIQUE KEY `codPrecio` (`codPrecio`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `precios` */

INSERT INTO `precios` (`codPrecio`, `servicio`, `precio`, `precioSocio`) VALUES 
('1', 'Inscripción en Partido', '15', '25'),
('2', 'Reserva de Pista', '10', '15'),
('4', 'Inscripción en Escuela', '15', '10'),
('5', 'Clase Particular', '25', '15'); 

/*Table structure for table `contenidodinamico` */

DROP TABLE IF EXISTS `contenidodinamico`;

CREATE TABLE `contenidodinamico` (
  `codContDin` INT(11) NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(50) COLLATE utf8_spanish_ci NOT NULL,
  `reseña` VARCHAR(255) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` DATE NOT NULL,
  `texto` TEXT COLLATE utf8_spanish_ci NOT NULL,
  `multimedia` VARCHAR(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `link` VARCHAR(244) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`codContDin`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `contenidodinamico` */

/* Table structure for table `reservasescuela` */

DROP TABLE IF EXISTS `reservasescuela`;

CREATE TABLE `reservasescuela` (
  `codEsc` INT(11) NOT NULL DEFAULT '0',
  `codReserva` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codEsc`,`codReserva`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

CREATE USER 'abp32'@'localhost' IDENTIFIED BY 'patata';
GRANT ALL PRIVILEGES ON `abp`.* TO 'abp32'@'localhost';
