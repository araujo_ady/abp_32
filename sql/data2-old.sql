/*
SQLyog Community
MySQL - 5.5.39 : Database - abp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

USE `abp`;

INSERT  INTO `campeonato`(`codCamp`,`nombre`,`fechaInicio`,`fechaFin`) VALUES 
(2,'Campeonato de Navidad','2019-12-20','2020-01-24');

insert  into `categoria`(`codCamp`,`codCategoria`,`nombre`) values 
(2,1,'masculino'),
(2,2,'femenina'),
(2,3,'mixta');

insert into `precios`(`codPrecio`,`servicio`,`precio`,`precioSocio`) VALUES
(1,'Venta de Patatas',4.99,3.99),
(2,'Alquiler de ornitorrincos',8.95,4.95);


insert  into `grupo`(`codCamp`,`codCategoria`,`codNivel`,`codGrupo`) values 
(2,1,1,1),
(2,1,2,1),
(2,1,3,1),
(2,2,1,1),
(2,2,2,1),
(2,2,3,1),
(2,3,1,1),
(2,3,2,1),
(2,3,3,1);

/*Table structure for table `jugadorespartido` */

DROP TABLE IF EXISTS `jugadorespartido`;

CREATE TABLE `jugadorespartido` (
  `jugador` int(11) NOT NULL DEFAULT '0',
  `partido` int(11) NOT NULL DEFAULT '0',
  `resultado` int(11) DEFAULT NULL,
  PRIMARY KEY (`jugador`,`partido`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `jugadorespartido` */

insert  into `jugadorespartido`(`jugador`,`partido`,`resultado`) values 
(32,1,0),
(33,2,0),
(34,2,0),
(35,3,0),
(36,3,0),
(37,4,0),
(38,4,0),
(39,4,0);


insert  into `nivel`(`codCamp`,`codCategoria`,`codNivel`,`nombre`) values 
(2,1,1,'amateur'),
(2,1,2,'Semi-profesional'),
(2,1,3,'Profesional'),
(2,2,1,'amateur'),
(2,2,2,'Semi-profesional'),
(2,2,3,'Profesional'),
(2,3,1,'amateur'),
(2,3,2,'Semi-profesional'),
(2,3,3,'Profesional');

insert  into `pareja`(`codCamp`,`codCategoria`,`codNivel`,`codGrupo`,`codUsr1`,`codUsr2`,`estaFormada`) values 
(1,1,1,1,5,6,1),
(1,1,1,1,7,8,1),
(1,1,1,1,9,10,1),
(1,1,1,1,11,60,1),
(1,1,1,1,12,59,1),
(1,1,1,1,13,58,1),
(1,1,1,1,14,57,1),
(1,1,1,1,15,56,1),
(1,1,1,1,16,55,1),
(1,1,1,1,17,54,1),
(1,1,1,1,18,53,1),
(1,1,1,1,19,52,1),
(1,1,1,1,20,51,1),
(1,1,1,1,21,50,1),
(1,1,1,1,22,49,1),
(1,1,1,1,23,48,1),
(2,1,1,1,1,47,1),
(2,1,1,1,2,46,1),
(2,1,1,1,26,45,1),
(2,1,1,1,27,44,1),
(2,1,1,1,28,43,1),
(2,1,1,1,29,42,1),
(2,1,1,1,30,41,1),
(2,1,1,1,31,40,1),
(2,1,1,1,32,39,1),
(2,1,1,1,33,38,1),
(2,1,1,1,34,37,1),
(2,1,1,1,35,36,1);


insert  into `partido`(`codPartido`,`fecha`,`hora`,`cancelado`,`promotor`,`codReserva`) values 
(1,'2019-11-23',5,0,0,0),
(2,'2019-11-23',6,0,0,0),
(3,'2019-11-24',4,0,0,0),
(4,'2019-11-24',7,0,0,0),
(5,'2019-11-23',2,0,0,0);


insert  into `pista`(`codPista`) values 
(1),
(2);


insert  into `reserva`(`codReserva`,`fecha`,`hora`,`codUsr`,`codPista`) values 
(1,'2019-11-23',1,6,2),
(2,'2019-11-23',2,7,2),
(3,'2019-11-23',3,8,2),
(4,'2019-11-23',4,9,2),
(5,'2019-11-23',5,10,2),
(6,'2019-11-23',6,11,2),
(7,'2019-11-23',7,12,2),
(8,'2019-11-24',0,13,2),
(9,'2019-11-24',1,14,2),
(10,'2019-11-24',2,15,2),
(11,'2019-11-24',3,16,2),
(12,'2019-11-24',4,17,2),
(13,'2019-11-24',5,18,2),
(14,'2019-11-24',6,19,2),
(15,'2019-11-24',7,20,2),
(16,'2019-11-23',0,21,1),
(17,'2019-11-23',1,22,1),
(18,'2019-11-23',3,23,1),
(19,'2019-11-23',4,24,1),
(20,'2019-11-23',7,25,1),
(21,'2019-11-24',0,26,1),
(22,'2019-11-24',1,27,1),
(23,'2019-11-24',2,28,1),
(24,'2019-11-24',3,29,1),
(25,'2019-11-24',5,30,1),
(26,'2019-11-24',6,31,1),
(27,'2019-11-23',34,5,1);

insert  into `usuario`(`codUsr`,`correo`,`password`,`nombre`,`apellidos`,`fechaNac`,`genero`,`esSocio`,`tipo`) values 
(1,'aagregorio@esei.uvigo.es','purple','Adrian','Araujo','1996-03-04','hombre',0,0),
(2,'avgarcia@esei.uvigo.es','purple','Alex','Villanueva','1996-07-05','hombre',0,0),
(3,'cfgarrido@esei.uvigo.es','purple','Cristhian','Ferreiro','1997-04-04','hombre',0,0),
(4,'jgmarchena@esei.uvigo.es','purple','Julio','Gonzalez','1991-03-04','hombre',1,0),
(5,'email@email.es','purple','Celso','Campos','1970-01-01','hombre',0,0),
(6,'email@email.es','purple','Pedro','Cuesta','1970-01-01','hombre',0,0),
(7,'email@email.es','purple','Arno','Formella','1970-01-01','hombre',0,0),
(8,'email@email.es','purple','Miguel Reboeiro','','1970-01-01','hombre',0,0),
(9,'email@email.es','purple','Francisco Jose','Ribadas','1970-01-01','hombre',0,0),
(10,'email@email.es','purple','Manuel','Vilares','1970-01-01','hombre',0,0),
(11,'email@email.es','purple','Sergio','Ramos','1970-01-01','hombre',0,0),
(12,'email@email.es','purple','Santiago','Cazorla','1970-01-01','hombre',0,0),
(13,'email@email.es','purple','David','Silva','1970-01-01','hombre',0,0),
(14,'email@email.es','purple','Amancio','Amaro','1970-01-01','hombre',0,0),
(15,'email@email.es','purple','Fabian','Ruiz','1970-01-01','hombre',0,0),
(16,'email@email.es','purple','Rafael','Nadal','1970-01-01','hombre',0,0),
(17,'email@email.es','purple','Daniil','Mevdevev','1970-01-01','hombre',0,0),
(18,'email@email.es','purple','Roberto','Bautista','1970-01-01','hombre',0,0),
(19,'email@email.es','purple','Pablo','Carreno','1970-01-01','hombre',0,0),
(20,'email@email.es','purple','Fernando','Verdasco','1970-01-01','hombre',0,0),
(21,'email@email.es','purple','Marc','Gasol','1970-01-01','hombre',0,0),
(22,'email@email.es','purple','Ricky','Rubio','1970-01-01','hombre',0,0),
(23,'email@email.es','purple','Luka','Doncic','1970-01-01','hombre',0,0),
(24,'email@email.es','purple','Sergio','Llull','1970-01-01','hombre',0,0),
(25,'email@email.es','purple','LeBron','James','1970-01-01','hombre',0,0),
(26,'email@email.es','purple','Francisco','Navarro','1970-01-01','hombre',0,0),
(27,'email@email.es','purple','Alejandro','Galan','1970-01-01','hombre',0,0),
(28,'email@email.es','purple','Carlos Daniel','Gutierrez','1970-01-01','hombre',0,0),
(29,'email@email.es','purple','Maximiliano','Sanchez','1970-01-01','hombre',0,0),
(30,'email@email.es','purple','Fernando','Belasteguin','1970-01-01','hombre',0,0),
(31,'email@email.es','purple','Gonzalo','Jacome','1970-01-01','hombre',0,0),
(32,'email@email.es','purple','Jesus','Vazquez','1970-01-01','hombre',0,0),
(33,'email@email.es','purple','Manuel','Cabezas','1970-01-01','hombre',0,0),
(34,'email@email.es','purple','Francisco','Rodriguez','1970-01-01','hombre',0,0),
(35,'email@email.es','purple','Jose Luis','Baltar','1970-01-01','hombre',0,0),
(36,'email@email.es','purple','Alejandro','Magno','1970-01-01','hombre',0,0),
(37,'email@email.es','purple','Julio','Cesar','1970-01-01','hombre',0,0),
(38,'email@email.es','purple','Napoleon','Bonaparte','1970-01-01','hombre',0,0),
(39,'email@email.es','purple','Hernan','Cortes','1970-01-01','hombre',0,0),
(40,'email@email.es','purple','Gengis','Khan','1970-01-01','hombre',0,0),
(41,'email@email.es','purple','Justin','Bieber','1970-01-01','hombre',0,0),
(42,'email@email.es','purple','Justin','Timberlake','1970-01-01','hombre',0,0),
(43,'email@email.es','purple','Drake','Graham','1970-01-01','hombre',0,0),
(44,'email@email.es','purple','Juan Luis','Maluma','1970-01-01','hombre',0,0),
(45,'email@email.es','purple','Zayn','Malik','1970-01-01','hombre',0,0),
(46,'email@email.es','purple','Rami','Malek','1970-01-01','hombre',0,0),
(47,'email@email.es','purple','Christian','Bale','1970-01-01','hombre',0,0),
(48,'email@email.es','purple','Bryan','Cranston','1970-01-01','hombre',0,0),
(49,'email@email.es','purple','Michael','Fassbender','1970-01-01','hombre',0,0),
(50,'email@email.es','purple','Matthew','McConaughey','1970-01-01','hombre',0,0),
(51,'email@email.es','purple','Monkey','Luffy','1970-01-01','hombre',0,0),
(52,'email@email.es','purple','edward','Elrick','1970-01-01','hombre',0,0),
(53,'email@email.es','purple','Light','Yagami','1970-01-01','hombre',0,0),
(54,'email@email.es','purple','Shinnosuke','Nohara','1970-01-01','hombre',0,0),
(55,'email@email.es','purple','Eren','Jaeger','1970-01-01','hombre',0,0),
(56,'email@email.es','purple','Anthony','Stark','1970-01-01','hombre',0,0),
(57,'email@email.es','purple','Miles','Morales','1970-01-01','hombre',0,0),
(58,'email@email.es','purple','Bruce','Banner','1970-01-01','hombre',0,0),
(59,'email@email.es','purple','Stephen','Strange','1970-01-01','hombre',0,0),
(60,'email@email.es','purple','Bruce','Wayne','1970-01-01','hombre',0,0),
(62,'admin@admin.es','purple','Admin','Admin','1970-10-01','hombre',1,2);

