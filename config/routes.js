/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/
  '/'                                        : 'ContenidoDinamicoController.showLasts',
  '/login'                                   : 'UsuarioController.login',
  'POST /login'                              : 'UsuarioController.auth',
  '/perfil'                                  : 'UsuarioController.perfil',
  '/logout'                                  : 'UsuarioController.logout',
  'GET /confirmarCambioPassword'             : 'UsuarioController.cambiarPassword',
  'POST /confirmarCambioPassword'            : 'UsuarioController.confirmCambiarPassword',
  '/recuperarPassword'                       : 'UsuarioController.recuperarPassword',
  'POST /recuperarPassword'                  : 'UsuarioController.confirmRecuperarPassword',

  /* GESTIONAR USUARIOS */
  '/gestionarusuarios'                       : 'UsuarioController.show',
  'GET /gestionarusuarios/nuevo'             : 'UsuarioController.insert',
  'POST /gestionarusuarios/nuevo'            : 'UsuarioController.confirmInsert',
  'GET /gestionarusuarios/modificar'         : 'UsuarioController.edit',
  'POST /gestionarusuarios/modificar'        : 'UsuarioController.confirmEdit',
  'GET /gestionarusuarios/cambiarPassword'   : 'UsuarioController.generateHashNewPassword',
  'GET /gestionarusuarios/borrar'            : 'UsuarioController.delete',
  'POST /gestionarusuarios/borrar'           : 'UsuarioController.confirmDelete',
  'POST /gestionarusuarios/cambiarAvatar'    : 'UsuarioController.cambiarAvatar',

  /*GESTION DE SOCIOS*/
  '/gestionarusuarios/altaSocio'                    : 'UsuarioController.altaSocio',
  'POST /gestionarusuarios/altaSocio'               : 'UsuarioController.confirmAltaSocio',
  '/gestionarusuarios/bajaSocio'                    : 'UsuarioController.bajaSocio',
  'POST /gestionarusuarios/bajaSocio'               : 'UsuarioController.confirmBajaSocio',
  'POST /gestionarusuarios/validarSocio'            : 'UsuarioController.validarSocio',
  'POST /gestionarusuarios/cancelarValidacion'      : 'UsuarioController.cancelarValidacion',


  /* GESTIONAR ESCUELA DEPORTIVA */
  '/gestionarescueladeportiva'                      : 'EscuelaDeportivaController.show',
  'GET /gestionarescueladeportiva/nuevo'            : 'EscuelaDeportivaController.insert',
  'POST /gestionarescueladeportiva/nuevo'           : 'EscuelaDeportivaController.confirmInsert',
  'GET /gestionarescueladeportiva/modificar'        : 'EscuelaDeportivaController.edit',
  'POST /gestionarescueladeportiva/modificar'       : 'EscuelaDeportivaController.confirmEdit',
  'GET /gestionarescueladeportiva/borrar'           : 'EscuelaDeportivaController.delete',
  'POST /gestionarescueladeportiva/borrar'          : 'EscuelaDeportivaController.confirmDelete',
  'GET /gestionarescueladeportiva/reservar'         : 'EscuelaDeportivaController.reservar',

  /* GESTIONAR INSCRIPCIÓN ESCUELA DEPORTIVA */
  '/gestionarInscripcionescueladeportiva'               : 'AlumnosEscuelaController.show',
  'GET /gestionarInscripcionescueladeportiva/nuevo'     : 'AlumnosEscuelaController.insert',
  'POST /gestionarInscripcionescueladeportiva/nuevo'    : 'AlumnosEscuelaController.insertInscripcion',
  'GET /gestionarInscripcionescueladeportiva/borrar'    : 'AlumnosEscuelaController.delete',
  'POST /gestionarInscripcionescueladeportiva/borrar'   : 'AlumnosEscuelaController.confirmDelete',
  '/misescuelas'                                        : 'AlumnosEscuelaController.showInscrito',

  /* GESTIONAR CLASES PARTICULARES */
  '/gestionarclaseparticular'                  : 'ClaseParticularController.show',
  'GET /gestionarclaseparticular/nuevo'        : 'ClaseParticularController.proponerFecha',
  'POST /gestionarclaseparticular/nuevo'       : 'ClaseParticularController.insert',
  '/misclasesparticulares'                     : 'ClaseParticularController.showInscrito',

  /* PROMOCIONAR PARTIDO */
  '/promocionarpartido'                 : 'PartidoController.show',
  '/promocionarpartido/nuevo'           : 'PartidoController.insert',
  'POST /promocionarpartido/nuevo'      : 'PartidoController.confirmInsert',
  'GET /promocionarpartido/modificar'   : 'PartidoController.edit',
  'POST /promocionarpartido/modificar'  : 'PartidoController.confirmEdit',
  'GET /promocionarpartido/borrar'      : 'PartidoController.delete',
  'POST /promocionarpartido/borrar'     : 'PartidoController.confirmDelete',

  /* INSCRIBIRSE EN PARTIDO PROMOCIONADO */
  '/inscribirseenpartido'                     : 'JugadoresPartidoController.showPartidosAbiertos',
  'POST /inscribirseenpartido/nuevo'          : 'JugadoresPartidoController.insert',
  'POST /inscribirseenpartido/confirmInsert'  : 'JugadoresPartidoController.confirmInsert',
  '/inscribirseenpartido/mispartidos'         : 'JugadoresPartidoController.show',
  'GET /inscribirseenpartido/borrar'          : 'JugadoresPartidoController.delete',
  'POST /inscribirseenpartido/borrar'         : 'JugadoresPartidoController.confirmDelete',

  /* GESTIONAR CAMPEONATO */
  '/gestionarcampeonatos'                : 'CampeonatoController.show',
  '/gestionarcampeonatos/nuevo'           : 'CampeonatoController.insert',
  'POST /gestionarcampeonatos/nuevo'      : 'CampeonatoController.confirmInsert',
  'GET /gestionarcampeonatos/modificar'   : 'CampeonatoController.edit',
  'POST /gestionarcampeonatos/modificar'  : 'CampeonatoController.confirmEdit',
  'GET /gestionarcampeonatos/borrar'      : 'CampeonatoController.delete',
  'POST /gestionarcampeonatos/borrar'     : 'CampeonatoController.confirmDelete',

  /* GESTIONAR INSCRIPCIÓN CAMPEONATO */
  '/gestionarInscripcionCampeonato'        : 'ParejaController.show' ,
  '/gestionarInscripcionCampeonato/nuevo'  : 'ParejaController.insert',
  '/gestionarInscripcionCampeonato/borrar' : 'ParejaController.delete',
  '/inscribirseEnCampeonato/confirmDelete' : 'ParejaController.confirmDelete',

  /* GESTIONAR LIGA REGULAR*/
  '/gestionarLigaRegular/categorias'            : 'LigaRegularController.categorias' ,
  '/gestionarLigaRegular/grupos'                : 'LigaRegularController.grupos' ,
  '/gestionarLigaRegular/enfretamientos'        : 'LigaRegularController.enfrentamientos' ,
  '/gestionarLigaRegular/generarenfrentamientos': 'LigaRegularController.generarLigaRegular',
  '/gestionarLigaRegular/modificarResultado'    : 'LigaRegularController.edit' ,
  '/gestionarLigaRegular/insertarResultado'     : 'LigaRegularController.confirmEdit' ,
  '/gestionarLigaRegular/parejas'               : 'LigaRegularController.parejas' ,
  '/gestionarLigaRegular/generarPlayOffs'       : 'LigaRegularController.generarPlayOffs',
  '/gestionarLigaRegular/verPlayOffs'           : 'LigaRegularController.verPlayOffs',

  /*ACORDAR PARTIDO CAMPEONATO*/
  '/misCampeonatos'                           : 'EnfrentamientoController.showCampeonatos',
  '/verMisEnfrentamientos'                    : 'EnfrentamientoController.showEnfrentamientos',
  '/proponerFecha'                            : 'EnfrentamientoController.elegirFecha',
  '/proponerFecha/insert'                     : 'EnfrentamientoController.insert',

  /*GESTIONAR NOTIFICACIONES*/
  '/gestionarNotificaciones'                  : 'NotificacionesController.show' ,
  'GET /gestionarNotificaciones/showDetail'   : 'NotificacionesController.showDetail',
  'GET /gestionarNotificaciones/enviar'       : 'NotificacionesController.insert',
  'POST /gestionarNotificaciones/enviar'      : 'NotificacionesController.confirmInsert',
  '/gestionarNotificaciones/aceptar'          : 'NotificacionesController.aceptar',
  '/gestionarNotificaciones/rechazar'         : 'NotificacionesController.rechazar',
  '/gestionarNotificaciones/aceptarClase'     : 'NotificacionesController.aceptarClase',
  '/gestionarNotificaciones/rechazarClase'    : 'NotificacionesController.rechazarClase',

  /*GESTIONAR RESERVAS*/
  '/gestionarreservas'                      : 'ReservaController.show',
  '/gestionarreservas/pista'                : 'ReservaController.eligePista',
  'GET /gestionarreservas/hora'             : 'ReservaController.eligeHora',
  'POST /gestionarreservas/nuevo'           : 'ReservaController.insert',
  'POST /gestionarreservas/save'            : 'ReservaController.confirmInsert',
  'GET /gestionarreservas/borrar'           : 'ReservaController.delete',
  'POST /gestionarreservas/borrar'          : 'ReservaController.confirmDelete',

  /*GESTIONAR PRECIOS*/
  '/gestionarprecios/'                       : 'PreciosController.show',
  'GET /gestionarprecios/modificar'         : 'PreciosController.edit',
  'POST /gestionarprecios/modificar'        : 'PreciosController.confirmEdit',

  /*GESTIONAR ESTADISTICAS*/
  '/verMisEstadisticas'                       : 'EstadisticasController.show',
  '/verClasificacion'                         : 'EstadisticasController.clasificacion',

  /*GESTIONAR AGENDA*/
  'GET /verMiAgenda'                       : 'AgendaController.show',

  /*GESTIONAR CONTENIDO DINÁMICO*/
  '/gestionarcontenido'                    : 'ContenidoDinamicoController.show',
  'GET /gestionarcontenido/showDetail'     : 'ContenidoDinamicoController.showDetail',
  '/gestionarcontenido/insert'             : 'ContenidoDinamicoController.insert',
  'POST /gestionarcontenido/insert'        : 'ContenidoDinamicoController.confirmInsert',
  'GET /gestionarcontenido/modificar'      : 'ContenidoDinamicoController.edit',
  'POST /gestionarcontenido/modificar'     : 'ContenidoDinamicoController.confirmEdit',
  'GET /gestionarcontenido/borrar'         : 'ContenidoDinamicoController.delete',
  'POST /gestionarcontenido/borrar'        : 'ContenidoDinamicoController.confirmDelete',

  /* GESTIONAR PISTAS */
  '/gestionarpistas'                       : 'PistaController.show',
  '/gestionarpistas/insert'                : 'PistaController.insert',
  'POST /gestionarpistas/insert'           : 'PistaController.confirmInsert',
  'GET /gestionarpistas/borrar'            : 'PistaController.delete',
  'POST /gestionarpistas/borrar'           : 'PistaController.confirmDelete'
};
