/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

  '*': 'Identificado',

  AlumnosEscuelaController: {
    '*': 'Identificado'
  },

  CampeonatoController: {
    '*': 'Administrador',
    'show': 'Identificado'
  },

  ClaseParticularController: {
    '*': 'Identificado'
  },

  EnfrentamientoController: {
    '*': 'Identificado'
  },

  EscuelaDeportivaController: {
    '*': 'Entrenador',
  },

  JugadoresPartidoController: {
    '*': 'Identificado'
  },

  LigaRegularController: {
    '*': 'Identificado',
    'generarLigaRegular': 'Administrador'
  },

  NotificacionesController: {
    '*': 'Identificado'
  },

  ParejaController: {
    '*': 'Identificado',
    'delete': 'Administrador'
  },

  PartidoController: {
    '*': 'Identificado'
  },

  PistaController: {
    '*': 'Administrador'
  },

  PreciosController: {
    '*': 'Administrador'
  },

  ReservaController: {
    '*': 'Identificado'
  },

  UsuarioController: {
    '*': 'Administrador',
    'perfil': 'Identificado',
    'generateHashNewPassword' : 'Identificado',
    'altaSocio' : 'Identificado',
    'confirmAltaSocio' : 'Identificado',
    'bajaSocio' : 'Identificado',
    'confirmBajaSocio' : 'Identificado',
    'cambiarAvatar'    : 'Identificado',
    'login': true,
    'auth': true,
    'logout': true,
    'cambiarPassword' : true,
    'confirmCambiarPassword' : true,
    'recuperarPassword' : true,
    'confirmRecuperarPassword' : true
  },

  AgendaController: {
    '*' : 'Entrenador'
  },

  ContenidoDinamicoController: {
    '*' : 'Administrador',
    'showLasts'  : true,
    'show'       : true,
    'showDetail' : true
  }
};
