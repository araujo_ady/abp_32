/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function() {
  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)
  // if (await User.count() > 0) {
  //   return;
  // }
  //
  // await User.createEach([
  //   { emailAddress: 'ry@example.com', fullName: 'Ryan Dahl', },
  //   { emailAddress: 'rachael@example.com', fullName: 'Rachael Shaw', },
  //   // etc.
  // ]);
  // ```

  // HAY QUE METERLES ESTO PARA QUE NO DEN LA LATA
  // ALTER TABLE `abp`.`categoria` DROP INDEX `codCategoria`;
  if(sails.config.models.migrate === 'drop'){ // Primary Key Múltiple en LigaRegular
    sails.sendNativeQuery('ALTER TABLE `abp`.`jugadorespartido` DROP INDEX `partido`, DROP PRIMARY KEY, ADD PRIMARY KEY (`jugador`, `partido`);').exec((err, res) => {
      if(!err){
        console.log('JugadoresPartido: Clave Primaria compuesta.');
      }
      else{
        console.log(err);
      }
    });
    sails.sendNativeQuery('ALTER TABLE `abp`.`categoria` DROP INDEX `codCategoria`, DROP PRIMARY KEY, ADD PRIMARY KEY (`codCamp`,`codCategoria`);').exec((err, res) => {
      if(!err){
        console.log('Categoria: Clave Primaria compuesta.');
      }
      else{
        console.log(err);
      }
    });
    sails.sendNativeQuery('ALTER TABLE `abp`.`nivel` DROP INDEX `codNivel`, DROP PRIMARY KEY, ADD PRIMARY KEY (`codCamp`,`codCategoria`,`codNivel`);').exec((err, res) => {
      if(!err){
        console.log('Nivel: Clave Primaria compuesta.');
      }
      else{
        console.log(err);
      }
    });
    sails.sendNativeQuery('ALTER TABLE `abp`.`grupo` DROP INDEX `codGrupo`, DROP PRIMARY KEY, ADD PRIMARY KEY (`codCamp`,`codCategoria`,`codNivel`, `codGrupo`);').exec((err, res) => {
      if(!err){
        console.log('Grupo: Clave Primaria compuesta.');
      }
      else{
        console.log(err);
      }
    });
    sails.sendNativeQuery('ALTER TABLE `abp`.`pareja` DROP INDEX `codCamp`, DROP PRIMARY KEY, ADD PRIMARY KEY (`codCamp`,`codCategoria`,`codNivel`, `codGrupo`, `codUsr1`, `codUsr2`);').exec((err, res) => {
      if(!err){
        console.log('Pareja: Clave Primaria compuesta.');
      }
      else{
        console.log(err);
      }
    });
    sails.sendNativeQuery('ALTER TABLE `abp`.`parejasenfrentamiento` DROP INDEX `codEnfrentamiento`, DROP PRIMARY KEY, ADD PRIMARY KEY (`codEnfrentamiento`,`codCamp`,`codCategoria`,`codNivel`,`codGrupo`,`capitan`,`jugador`);').exec((err, res) => {
      if(!err){
        console.log('ParejasEnfrentamiento: Clave Primaria compuesta.');
      }
      else{
        console.log(err);
      }
    });
    sails.sendNativeQuery('ALTER TABLE `abp`.`alumnosescuela` DROP INDEX `escuela`, DROP PRIMARY KEY, ADD PRIMARY KEY (`alumno`,`escuela`);').exec((err, res) => {
      if(!err){
        console.log('AlumnosEscuela: Clave Primaria compuesta.');
      }
      else{
        console.log(err);
      }
    });
    sails.sendNativeQuery('ALTER TABLE `abp`.`reservasescuela` DROP INDEX `codReserva`, DROP PRIMARY KEY, ADD PRIMARY KEY (`codEsc`,`codReserva`);').exec((err, res) => {
      if(!err){
        console.log('ReservasEscuela: Clave Primaria compuesta.');
      }
      else{
        console.log(err);
      }
    });
  }
};
